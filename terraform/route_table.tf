# RT for public subnet
resource "aws_route_table" "gw" {
  vpc_id = aws_vpc.main-vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }
  tags = {
    Name  = var.vpc_name
    Owner = var.owner
  }
}
# RT for NAT1
resource "aws_route_table" "private_1" {
  vpc_id = aws_vpc.main-vpc.id
  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat_gw1.id
  }
  tags = {
    Name  = var.vpc_name
    Owner = var.owner
  }
}
# RT for NAT2
resource "aws_route_table" "private_2" {
  vpc_id = aws_vpc.main-vpc.id
  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat_gw2.id
  }
  tags = {
    Name  = var.vpc_name
    Owner = var.owner
  }
}
