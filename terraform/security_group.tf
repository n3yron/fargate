resource "aws_security_group" "default" {
  name        = "allow_tls"
  description = "Allow some inbound traffic"
  vpc_id      = aws_vpc.main-vpc.id
  ingress {
    description = "HTTPS"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  tags = {
    VPC_Name = var.vpc_name
    Owner    = var.owner
    Dept     = var.dept_id
  }
}
