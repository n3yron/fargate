# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/availability_zones
# Declare the data source
data "aws_availability_zones" "available" {
  state = "available"
}
### 2 Public subnets ###
locals {
  public_subnets_locals = {
    "public_subnet_1" = { cidr_block = "10.0.1.0/24", availability_zone = data.aws_availability_zones.available.names[0] },
    "public_subnet_2" = { cidr_block = "10.0.2.0/24", availability_zone = data.aws_availability_zones.available.names[1] }
  }
}

resource "aws_subnet" "public_subnets" {
  for_each                = { for key, value in local.public_subnets_locals : key => value }
  vpc_id                  = aws_vpc.main-vpc.id
  cidr_block              = each.value.cidr_block
  availability_zone       = each.value.availability_zone
  map_public_ip_on_launch = true
  tags = {
    Name     = each.key
    VPC_Name = var.vpc_name
    Owner    = var.owner
    Dept     = var.dept_id
    Type     = "public"
    # https://docs.aws.amazon.com/eks/latest/userguide/alb-ingress.html
    "kubernetes.io/cluster/${var.my_cluster_name}" = "shared"
    "kubernetes.io/role/elb"                       = 1
  }
}
### 2 Private subnets ###
locals {
  private_subnets_locals = {
    "private_subnet_1" = { cidr_block = "10.0.3.0/24", availability_zone = data.aws_availability_zones.available.names[0] },
    "private_subnet_2" = { cidr_block = "10.0.4.0/24", availability_zone = data.aws_availability_zones.available.names[1] }
  }
}

resource "aws_subnet" "private_subnets" {
  for_each          = { for key, value in local.private_subnets_locals : key => value }
  vpc_id            = aws_vpc.main-vpc.id
  cidr_block        = each.value.cidr_block
  availability_zone = each.value.availability_zone
  tags = {
    Name     = each.key
    VPC_Name = var.vpc_name
    Owner    = var.owner
    Dept     = var.dept_id
    Type     = "private"
    # https://docs.aws.amazon.com/eks/latest/userguide/alb-ingress.html
    "kubernetes.io/cluster/${var.my_cluster_name}" = "shared"
    "kubernetes.io/role/internal-elb"              = 1
  }
}
