# Not working on Fargate as using classic LB
#resource "helm_release" "ingress-nginx" {
#  name = "nginx-ingress-controller"

#  repository = "https://kubernetes.github.io/ingress-nginx"
#  chart      = "ingress-nginx"
#  namespace  = "staging"
#  depends_on = [aws_eks_cluster.eks_cluster]
#}

provider "helm" {
  kubernetes {
    host                   = data.aws_eks_cluster.example.endpoint
    cluster_ca_certificate = base64decode(data.aws_eks_cluster.example.certificate_authority[0].data)
    token                  = data.aws_eks_cluster_auth.example.token
  }
}
