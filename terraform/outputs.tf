
output "vpc_id" {
  value     = aws_vpc.main-vpc.id
  sensitive = false
}
# Output AMI ID and name
#output "ami_name" {
#  value     = data.aws_ami.my_image.name
#  sensitive = false
#}
#output "ami_id" {
#  value     = data.aws_ami.my_image.id
#  sensitive = false
#}
#output "keys" {
#  value     = data.aws_key_pair.key_pair.id
#  sensitive = false
#}
output "cluster_name" {
  value = local.cluster_name
}

output "endpoint" {
  value = data.aws_eks_cluster.example.endpoint
}

output "kubeconfig-certificate-authority-data" {
  value = data.aws_eks_cluster.example.certificate_authority[0].data
}

# Only available on Kubernetes version 1.13 and 1.14 clusters created or upgraded on or after September 3, 2019.
output "token" {
  value     = data.aws_eks_cluster_auth.example.token
  sensitive = true
}
output "aws_efs_id" {
  value     = aws_efs_file_system.eks.id
  sensitive = false
}
#resource "local_file" "rds_record_file" {
#  content    = data.aws_db_instance.database.address
#  filename   = "/tmp/rds_record.txt"
#  depends_on = [aws_db_instance.rds]
#}
