# Kubernetes provider
# https://learn.hashicorp.com/terraform/kubernetes/provision-eks-cluster#optional-configure-terraform-kubernetes-provider
# To learn how to schedule deployments and services using the provider, go here: https://learn.hashicorp.com/terraform/kubernetes/deploy-nginx-kubernetes

# The Kubernetes provider is included in this file so the EKS module can complete successfully. Otherwise, it throws an error when creating `kubernetes_config_map.aws_auth`.
# You should **not** schedule deployments and services in this workspace. This keeps workspaces modular (one for provision EKS, another for scheduling Kubernetes resources) as per best practices.
data "aws_eks_cluster" "example" {
  name       = var.my_cluster_name
  depends_on = [aws_eks_cluster.eks_cluster]
}
data "aws_eks_cluster_auth" "example" {
  name       = var.my_cluster_name
  depends_on = [aws_eks_cluster.eks_cluster]
}
provider "kubernetes" {
  host                   = data.aws_eks_cluster.example.endpoint
  token                  = data.aws_eks_cluster_auth.example.token
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.example.certificate_authority.0.data)
}

#provider "helm" {
#  kubernetes {
#    host                   = data.aws_eks_cluster.example.endpoint
#    cluster_ca_certificate = base64decode(data.aws_eks_cluster.example.certificate_authority[0].data)
#    token                  = data.aws_eks_cluster_auth.example.token
#  }
#}
