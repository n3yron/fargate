# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table_association
resource "aws_route_table_association" "public_1" {
  subnet_id      = aws_subnet.public_subnets["public_subnet_1"].id
  route_table_id = aws_route_table.gw.id # as we have count
}
resource "aws_route_table_association" "public_2" {
  subnet_id      = aws_subnet.public_subnets["public_subnet_2"].id
  route_table_id = aws_route_table.gw.id
}
# Private
resource "aws_route_table_association" "private_1" {
  subnet_id      = aws_subnet.private_subnets["private_subnet_1"].id
  route_table_id = aws_route_table.private_1.id # as we have count
}
resource "aws_route_table_association" "private_2" {
  subnet_id      = aws_subnet.private_subnets["private_subnet_2"].id
  route_table_id = aws_route_table.private_2.id # as we have count
}
