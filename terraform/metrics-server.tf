#resource "helm_release" "ingress-nginx" {
#  name = "nginx-ingress-controller"
#
#  repository = "https://kubernetes.github.io/ingress-nginx"
#  chart      = "ingress-nginx"
#
#  depends_on = [aws_eks_cluster.eks_cluster]
#}

resource "helm_release" "metrics-server" {
  name = "metrics-server"

  repository = "https://kubernetes-sigs.github.io/metrics-server/"
  chart      = "metrics-server"
  namespace  = "kube-system"
  version    = "3.8.2"

  set {
    name  = "metrics.enabled"
    value = false
  }

  depends_on = [aws_eks_fargate_profile.kube-system, aws_eks_cluster.eks_cluster, data.aws_eks_cluster_auth.example]
}

#resource "helm_release" "prometheus" {
#  name = "prometheus"

#  repository = "https://github.com/prometheus-community/helm-charts/tree/main/charts/prometheus"
#  chart      = "prometheus-community/prometheus"
#  namespace  = "kube-system"
#  version    = "3.8.2"

#  set {
#    name  = "metrics.enabled"
#    value = false
#  }

#  depends_on = [aws_eks_fargate_profile.kube-system]
#}
