# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/eks_fargate_profile
resource "aws_eks_fargate_profile" "staging" {
  cluster_name           = aws_eks_cluster.eks_cluster.name
  fargate_profile_name   = "staging"
  pod_execution_role_arn = aws_iam_role.eks-fargate.arn
  subnet_ids             = [aws_subnet.private_subnets["private_subnet_1"].id, aws_subnet.private_subnets["private_subnet_2"].id]

  selector {
    namespace = "staging"
  }
}
resource "aws_eks_fargate_profile" "monitoring" {
  cluster_name           = aws_eks_cluster.eks_cluster.name
  fargate_profile_name   = "monitoring"
  pod_execution_role_arn = aws_iam_role.eks-fargate.arn
  subnet_ids             = [aws_subnet.private_subnets["private_subnet_1"].id, aws_subnet.private_subnets["private_subnet_2"].id]

  selector {
    namespace = "monitoring"
  }
}

resource "aws_eks_fargate_profile" "default" {
  cluster_name           = aws_eks_cluster.eks_cluster.name
  fargate_profile_name   = "default"
  pod_execution_role_arn = aws_iam_role.eks-fargate.arn
  subnet_ids             = [aws_subnet.private_subnets["private_subnet_1"].id, aws_subnet.private_subnets["private_subnet_2"].id]

  selector {
    namespace = "default"
  }
}
resource "aws_eks_fargate_profile" "prometheus" {
  cluster_name           = aws_eks_cluster.eks_cluster.name
  fargate_profile_name   = "prometheus"
  pod_execution_role_arn = aws_iam_role.eks-fargate.arn
  subnet_ids             = [aws_subnet.private_subnets["private_subnet_1"].id, aws_subnet.private_subnets["private_subnet_2"].id]

  selector {
    namespace = "prometheus"
  }
}
