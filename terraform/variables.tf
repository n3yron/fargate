variable "cidr" {
  default = "10.0.0.0/16"
}
locals {
  region = "us-east-2"
}
variable "owner" {
  default = "oleksandr_dovnich_sre"
}
variable "dept_id" {
  default = "4566"
}
variable "rt" {
  default = "1"
}
variable "private_subs" {
  default = "1"
}
variable "deploy_private_subnets" {
  default     = "1"
  description = "If 1, private subnets, private route tables, private rt associations, nat gateways, eips for nat gateways will be created. If 0 - not."
}
variable "my_cluster_name" {
  default = "n3yron-eks"
}
#data "aws_availability_zones" "available" {}
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/availability_zones
locals {
  cluster_name = var.my_cluster_name
}
variable "region" {
  default     = "us-east-2"
  description = "AWS region"
}
########
variable "cluster_name" {
  default = "n3yron-eks"
}
variable "account_aws" {
  default = "421572644019"
}
variable "vpc_name" {
  default = "n3yron-vpc"
}
