resource "aws_vpc" "main-vpc" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags = {
    Name  = "${var.my_cluster_name}-vpc"
    Owner = var.owner
    Dept  = var.dept_id
  }
}
