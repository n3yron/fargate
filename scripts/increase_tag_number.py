import subprocess
output = subprocess.Popen(['sh', '-c', 'git tag | tail -1'], stdout=subprocess.PIPE).communicate()
clean_line = output[0].strip().decode( "utf-8" )
version = clean_line.split('.')
###
version = '3.0.0'
version = version.split('.')

if int(version[2]) < 99:
    print(version[0]+'.'+version[1]+'.'+str(int(version[2])+1))
elif int(version[1]) < 9: # esli .3=99
    print(version[0]+'.'+str(int(version[1])+1)+'.'+'0')
else: # num2=9, num3=99
    print(str(int(version[0])+1)+'.'+'0'+'.'+'0')