import re

# Define the regular expression pattern
pattern = r'BEGIN BLOCK(.+?)END BLOCK'

# Define the replacement string
replacement = 'new code block'

# Define the input string
input_string = '''
ewfwefe

BEGIN BLOCK
old code block
wefewfwef
ewfwefefewfewf
ewfwefe
wefewfwefewfwef
END BLOCK

kukudu
'''

# Use the sub() method to substitute the matched block of code
output_string = re.sub(pattern, replacement, input_string, flags=re.DOTALL)

# Print the result
print(output_string)