import re
import os
import json
######
#full_dict = {}
#new_dict = {}
#unique_keys = set()

folder_path = "environments/"

def dev_env():
    for filename in os.listdir(folder_path):
        if filename == "dev.tfvars":
            filepath = os.path.join(folder_path, filename)
            make_changes(filepath)

def uat_env():
    for filename in os.listdir(folder_path):
        if filename == "uat.tfvars":
            filepath = os.path.join(folder_path, filename)
            make_changes(filepath)

def prod_env():
    for filename in os.listdir(folder_path):
        if filename == "prod.tfvars":
            filepath = os.path.join(folder_path, filename)
            make_changes(filepath)
################################################################

def make_changes(filepath):
    full_dict = {}
    new_dict = {}
    unique_keys = set()
    with open(filepath, 'r') as f:
        #text1 = f.read()
        text = f.readlines()
        for line in text:
            if "AWS-" in line:
                # Remove any spaces from the line using the replace method
                line = line.replace(" ", "")
                line = line.replace('"', '')
                line = line.replace(',', '')
                key, value = line.split("=")
                full_dict[key] = value

    for key in full_dict:
        full_dict[key] = [full_dict[key]]
    for key, value in full_dict.items():
        key = re.sub(r"\d+_", "", key)
        if key not in unique_keys:
            unique_keys.add(key)
            new_dict[key] = value
        else:
            for i in value:
                new_dict[key].append(i)
    #print(new_dict)
    replacement1 = ""
    string = "sso_permission_sets = ["
    with open(filepath, 'r') as f:
        text1 = f.read()
    with open(filepath, 'w') as f1:
        pattern = r'sso_permission_sets = .+= "(dev|prod|uat)"'
        for key, value in new_dict.items():
            #print(value)
            value = str(value).replace("\n", "")
            replacement = '\n{\n  group = "'+key+'"\n  permission_sets = '+str(value).rstrip()+'\n},'
            string += replacement
        string = string.replace('\\n', '')
        string = string.replace("'", '"')
        string += "\n]"
        print(str(string))
        output_string = re.sub(pattern, string, text1, flags=re.DOTALL)
        f1.write(output_string)
        print(filepath)
        return 'lol'

dev_env()
uat_env()
#prod_env()
