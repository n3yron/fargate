enable_backup_vault       = "true"
network_provider_role_arn = "arn:aws:lam::877607016832:role/pg-role-gitlab-infra-automation"
provider_role_arn         = "arn:aws:lam::147800237654:role/pg-role-gitlab-infra-automation"
region                    = "eu-central-1"
vpc_cidr                  = "10.57.18.0/24"
vpc_stage                 = "dev"
sso_permission_sets = [
{
  group = "AWS-PIM-Admins"
  permission_sets = ["pg-sso-administrator"]
},
{
  group = "rIALLIWAppP-AWS-NetworkAdmin"
  permission_sets = ["pg-sso-ololo", "pg-sso-networkadministrator", "pg-sso-readonly", "pg-sso-monitoring", "pg-sso-billing"]
},
{
  group = "rIALLIWAppP-AWS-PG-PlatformOwner"
  permission_sets = ["pg-sso-owner"]
},
{
  group = "rIALLIWAppP-AWS-PG-buap-clm-services-pg-ProdOwner"
  permission_sets = ["pg-sso-app-data"]
},
]
enabled_map_tags = true
