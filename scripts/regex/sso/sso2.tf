sso_permission_sets = [
  {
    group           = "00_AWS-PIM-Admins"
    permission_sets = ["pg-sso-administrator"]
  },
  {
    group           = "14_rIALLWAppP-AWS-NetworkAdmin"
    permission_sets = ["pg-sso-networkadministrator"]
  },
  {
    group           = "15_rIALLWWAppP-AWS-NetworkAdmin"
    permission_sets = ["pg-sso-readonly"]
  },
  {
    group           = "17_rIALLIWAppP-ANS-PG-PlatformOwner"
    permission_sets = ["pg-sso-readonly", "pg-sso-billing"]
  },
]
