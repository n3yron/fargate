import re
import os
import json
######
folder_path = "environments/"
def each_env():
    file_dict = {"prod.tfvars": "prod", "uat.tfvars": "uat", "dev.tfvars": "dev"}
    for filename in os.listdir(folder_path):
        filepath = os.path.join(folder_path, filename)
        if filename in file_dict:
            #print(file_dict[filename])
            env_name = file_dict[filename]
            make_changes(filepath, env_name)
########
def make_changes(filepath, env_name):
    with open(filepath, 'r') as f:
        text = f.read()

    full_dict = {}
    new_dict = {}
    unique_keys = set()
    # Split the text into lines using the split method
    lines = text.split("\n")
    # Iterate over the lines and split each line into key and value using the split method
    for line in lines:
        if "AWS-" in line:
            # Remove any spaces from the line using the replace method
            line = line.replace(" ", "")
            line = line.replace('"', '')
            line = line.replace(',', '')
            key, value = line.split("=")
            full_dict[key] = value
    ################################################################################
    for key in full_dict:
        full_dict[key] = [full_dict[key]]
    #print(full_dict)
    #unique_keys = set()
    for key, value in full_dict.items():
        key = re.sub(r"\d+_", "", key)
        if key not in unique_keys:
            unique_keys.add(key)
            new_dict[key] = value
        else:
            for i in value:
                new_dict[key].append(i)
    replacement1 = ""
    with open(filepath, 'w') as f:
        pattern = r'sso_permission_sets = .+= "(dev|prod|uat)"'
        output_string = re.sub(pattern, replacement1, text, flags=re.DOTALL)
        f.write(output_string)
        f.write("\nsso_permission_sets = [")
#    with open(filepath, 'w') as f:
        for key, value in new_dict.items():
            replacement = '\n{\n  group = "'+key+'"\n  permission_sets = '+str(value)+'\n},'
            #repl = repl.append(replacement)
            replacement = replacement.replace("'", '"')
            print(replacement)
            f.write(replacement)
        f.write('\n]\nenvironment_name = "'+str(env_name)+'"')

each_env()