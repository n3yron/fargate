import os
import re

replacement1 = "lol"

with open('sso.tf', 'r') as f:
    text1 = f.read()
#    pattern = r"sso_permission_sets(.+?)\n}"
    pattern = r'sso_permission_sets = .+= "(dev|prod|uat)"'
    output_string = re.sub(pattern, replacement1, text1, flags=re.DOTALL)
    print(output_string)