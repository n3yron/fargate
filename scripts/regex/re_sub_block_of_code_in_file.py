import re

# Define the regular expression pattern
pattern = r"terraform {\n\s+backend \"s3\"(.+?)\"terraform-backend-lambda-n3yron\".*?\n\s+}\n}"


# Define the replacement string
replacement = '''terraform {
  backend "s3" {
    bucket         = "terraform-backend-lambda-n3yron"
    key            = "cloudwatch/terraform.tfstate"
    region         = "us-west-2"
    dynamodb_table = "backend-lambda-n3yron"
  }
}

terraform {
  backend "s3" {
    bucket         = "example"
    key            = "example"
    region         = "example"
    dynamodb_table = "example"
  }
}
'''

# Open the file and read its contents
with open('test.tf', 'r') as file:
    input_string = file.read()

# Use the sub() method to substitute the matched block of code
output_string = re.sub(pattern, replacement, input_string, flags=re.DOTALL)

# Open the file again and write the modified contents
#with open('output_file.txt', 'w') as file:
#    file.write(output_string)
print(output_string)