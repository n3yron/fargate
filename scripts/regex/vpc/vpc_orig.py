# if main.tf in folder, check if there vpc {. If yes, create networking.tf and move vpc block code there
# All to do if vpc in main!!!!!!!
import re
import os
from pathlib import Path

var_vpc_main_region = '''variable "vpc_main_region" {
  description = "Map for VPC configuration of the main region"
  type = set(object({
    cidrs    = list(string),
    az_count = number
  }))
  default = null
}
'''

# main.tf
def move_vpc_module_in_networking():
    my_file = Path('main.tf')
    if my_file.is_file(): # if main.tf exists
        with open(my_file, 'r') as f:
            text = f.read()
            if 'module "vpc"' in text: # if vpc in main.tf
                #print(text)
                pattern = r'\nmodule "vpc" {.+?aws\.network\n\s*}\n\s*}\n'
                match = re.search(pattern, text, re.DOTALL)
                if match:
                    cut_text = match.group()
                    with open('networking.tf', 'w') as f:
                        f.write(cut_text)
                    #print(block_of_text)
                    output_string = re.sub(pattern, '', text, flags=re.DOTALL)
                    with open('main1.tf', 'w') as f:
                        f.write(output_string)
                    #print(output_string)
    with open('common.auto.tfvars', 'a') as f:
        f.write('\nmain_region = "eu-central-1"')

def region_in_main_region():
    # dev.tfvars, uat.tfvars, prod.tfvars block
    folder_path = "environments/"
    file_list = ["prod.tfvars", "uat.tfvars", "dev.tfvars"]
    for filename in os.listdir(folder_path):
        filepath = os.path.join(folder_path, filename)
        # Check if the file is in the dictionary
        if filename in file_list:
            make_changes_in_tfvars(filepath)
    # providers.tf, variables.tf block
    file_list2 = ["providers.tf", "variables.tf"]
    for filename in os.listdir("./"):
        if filename in file_list2:
            #print(filename)
            #filename = filename
            make_changes_in_files(filename)

def make_changes_in_tfvars(filepath): # dev.tfvars, uat.tfvars, prod.tfvars
    with open(filepath, 'r') as f:
        output1 = f.read()
        if "vpc_main_region" in output1:
            pattern1 = r'\nregion\s*=\s*"eu-central-1"'
            output_string1 = re.sub(pattern1, '', output1, flags=re.DOTALL)
            with open(filepath+'_1', 'w') as f:
                f.write(output_string1)
        else:
            pattern1 = r'\nregion\s*=\s*"eu-central-1"'
            output_string1 = re.sub(pattern1, '', output1, flags=re.DOTALL)
            ### vpc_az_count
            pattern1 = r'\nvpc_az_count\s*=\s*("\d+"|\d+)'
            match = re.search(pattern1, output_string1, re.DOTALL)
            if match:
                cut_text = match.group()
                #print(cut_text)
                key, value = cut_text.split("=")
                vpc_az_count = value.replace('"', '')
                #print(vpc_az_count)
            output_string1 = re.sub(pattern1, '', output_string1, flags=re.DOTALL)
            pattern1 = r'\nvpc_cidr\s*=\s*"\d+.\d+.\d+.\d+/\d+"'
            cidr_pattern = r'\d+.\d+.\d+.\d+/\d+'
            match = re.search(cidr_pattern, output_string1, re.DOTALL)
            if match:
                cidr = match.group()
                #print(cidr)
            output_string1 = re.sub(pattern1, '', output_string1, flags=re.DOTALL)
            vpc_main_region = "vpc_main_region = [\n  {\n  cidrs = [\"" + cidr + "\"],\n  az_count = " +vpc_az_count+ "\n  }\n]"
            pattern1 = r'vpc_stage\s*=\s*"(dev|prod|uat)"'
            match = re.search(pattern1, output_string1, re.DOTALL)
            if match:
                vpc_stage = match.group()
                #print(vpc_stage)
            output_string1 = re.sub(pattern1, vpc_main_region + "\n" + vpc_stage, output_string1, flags=re.DOTALL)
            with open(filepath+'_1', 'w') as f:
                f.write(output_string1)


def make_changes_in_files(filename):
    with open(filename, 'r') as f: # if providers, need to avoid region change
        if filename == "variables.tf": # "variables.tf"
            #print(filename)
            variables_output = f.read()
            variables_pattern = r'variable "region"' # +
            if "vpc_main_region" not in variables_output:
                variables_string = re.sub(variables_pattern, var_vpc_main_region+'\nvariable "main_region"', variables_output, flags=re.DOTALL)
                #with open(filename+'_1', 'w') as f:
                #    f.write(variables_string)
            else:
                variables_string = re.sub(variables_pattern, 'variable "main_region"', variables_output, flags=re.DOTALL)
                #with open(filename+'_1', 'w') as f:
                #    f.write(variables_string)
            with open(filename+'_1', 'w') as f:
                f.write(variables_string)
        else: # "providers.tf"
            providers_output = f.read()
            providers_pattern = r'var.region'
            providers_string = re.sub(providers_pattern, 'var.main_region', providers_output, flags=re.DOTALL)
            with open(filename+'_1', 'w') as f:
                f.write(providers_string)


def main():
    move_vpc_module_in_networking()
    region_in_main_region()

main()

# Need to make original names of files, to be able to change originals. For example main.tf. Also need to check if some files also contain region var
# add vpc_main_region in variables. If core.tf, vpc_main_region already exists, need only change region var on main_region
# Better! if no vpc_main_region!