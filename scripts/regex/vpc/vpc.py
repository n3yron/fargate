# if main.tf in folder, check if there vpc {. If yes, create networking.tf and move vpc block code there
# All to do if vpc in main!!!!!!!
import re
import os
from pathlib import Path

var_vpc_main_region = '''variable "vpc_main_region" {
  description = "Map for VPC configuration of the main region"
  type = set(object({
    cidrs    = list(string),
    az_count = number
  }))
  default = null
}
'''

add_vpc_locals = '''locals {
  vpc_main_region = flatten([
    for object_element in var.vpc_main_region : [
      for cidr in object_element.cidrs :
      {
        "az_count" = object_element.az_count
        "cidr"     = cidr
      }
    ]
  ])
}
module "vpc" {
  for_each = {
    for k in local.vpc_main_region : k.cidr => k
  }
'''

vpc_params = '''\n\n  vpc_cidr = each.key
  vpc_stage = var.vpc_stage
  vpc_az_count = each.value.az_count
'''

# main.tf
def move_vpc_module_in_networking():
    my_file = Path('main.tf')
    if my_file.is_file(): # if main.tf exists
        with open(my_file, 'r') as f:
            text = f.read()
            if 'module "vpc"' in text: # if vpc in main.tf
                #print(text)
                pattern = r'\nmodule "vpc" {.+?aws\.network\n\s*}\n\s*}\n'
                match = re.search(pattern, text, re.DOTALL)
                if match:
                    cut_text = match.group()
                    local_pattern = r'module "vpc" {'
                    cut_text = re.sub(local_pattern, add_vpc_locals, cut_text, flags=re.DOTALL)
                    vpc_params_pattern = r'\s*vpc_cidr\s*= var.vpc_cidr\n\s*vpc_stage\s*= var.vpc_stage\n\s*vpc_az_count\s*= var.vpc_az_count'
                    cut_text = re.sub(vpc_params_pattern, vpc_params, cut_text, flags=re.DOTALL)
                    print(cut_text)
                    with open('networking.tf', 'w') as f:
                        f.write(cut_text)
                    #print(block_of_text)
                    output_string = re.sub(pattern, '', text, flags=re.DOTALL)
                    with open(my_file, 'w') as f: #my_file
                        f.write(output_string)
                    #print(output_string)
    with open('common.auto.tfvars', 'a') as f:
        f.write('\nmain_region = "eu-central-1"')

def region_in_main_region():
    # dev.tfvars, uat.tfvars, prod.tfvars block
    folder_path = "environments/"
    file_list = ["prod.tfvars", "uat.tfvars", "dev.tfvars"]
    for filename in os.listdir(folder_path):
        filepath = os.path.join(folder_path, filename)
        # Check if the file is in the dictionary
        if filename in file_list:
            make_changes_in_tfvars(filepath)
    # providers.tf, variables.tf block
    file_list2 = ["providers.tf", "variables.tf"]
    for filename in os.listdir("./"):
        if filename in file_list2:
            #print(filename)
            #filename = filename
            make_changes_in_files(filename)

def make_changes_in_tfvars(filepath): # dev.tfvars, uat.tfvars, prod.tfvars
    vpc_az_count = ""
    cidr = ""
    vpc_stage = ""
    with open(filepath, 'r') as f:
        output1 = f.read()
        if "vpc_main_region" in output1:
            pattern1 = r'\nregion\s*=\s*"eu-central-1"'
            output_string1 = re.sub(pattern1, '', output1, flags=re.DOTALL)
            with open(filepath, 'w') as f:
                f.write(output_string1)
        else:
            pattern1 = r'\nregion\s*=\s*"eu-central-1"'
            output_string1 = re.sub(pattern1, '', output1, flags=re.DOTALL)
            ### vpc_az_count
            pattern1 = r'\nvpc_az_count\s*=\s*("\d+"|\d+)'
            match = re.search(pattern1, output_string1, re.DOTALL)
            if match:
                cut_text = match.group()
                #print(cut_text)
                key, value = cut_text.split("=")
                vpc_az_count = value.replace('"', '')
            if len(vpc_az_count) < 1:
                vpc_az_count = '2'
                print(vpc_az_count)
            output_string1 = re.sub(pattern1, '', output_string1, flags=re.DOTALL)
            pattern1 = r'\nvpc_cidr\s*=\s*"\d+.\d+.\d+.\d+/\d+"'
            cidr_pattern = r'\d+.\d+.\d+.\d+/\d+'
            match = re.search(cidr_pattern, output_string1, re.DOTALL)
            if match:
                cidr = match.group()
                #print(cidr)
            output_string1 = re.sub(pattern1, '', output_string1, flags=re.DOTALL)
            vpc_main_region = "vpc_main_region = [\n  {\n  cidrs = [\"" + cidr + "\"],\n  az_count = " +vpc_az_count+ "\n  }\n]"
            pattern1 = r'vpc_stage\s*=\s*"(dev|prod|prd|uat)"'
            match = re.search(pattern1, output_string1, re.DOTALL)
            if match:
                vpc_stage = match.group()
                #print(vpc_stage)
            output_string1 = re.sub(pattern1, vpc_main_region + "\n" + vpc_stage, output_string1, flags=re.DOTALL)
            with open(filepath, 'w') as f:
                f.write(output_string1)


def make_changes_in_files(filename):
    with open(filename, 'r') as f: # if providers, need to avoid region change
        if filename == "variables.tf": # "variables.tf"
            #print(filename)
            variables_output = f.read()
            variables_pattern = r'variable "region"' # if vpc_main_region exists, this means vpc_cidr not exists
            if "vpc_main_region" not in variables_output:
                cidr_pattern = r'\nvariable "vpc_cidr" {\n\s*description = "Provide vpc cidr block"\n\s*type\s*= string\n}'
                vpc_az_cont_patten = r'\nvariable "vpc_az_count" {\n\s*description\s*= "Provide number of az to use"\n\s*type\s*= number\n\s*default\s*= "2"\n}'
                variables_string = re.sub(variables_pattern, var_vpc_main_region+'\nvariable "main_region"', variables_output, flags=re.DOTALL)
                variables_string = re.sub(cidr_pattern, '', variables_string, flags=re.DOTALL)
                variables_string = re.sub(vpc_az_cont_patten, '', variables_string, flags=re.DOTALL)
            else:
                variables_string = re.sub(variables_pattern, 'variable "main_region"', variables_output, flags=re.DOTALL)
            with open(filename, 'w') as f:
                f.write(variables_string)
        else: # "providers.tf"
            providers_output = f.read()
            providers_pattern = r'var.region'
            providers_string = re.sub(providers_pattern, 'var.main_region', providers_output, flags=re.DOTALL)
            with open(filename, 'w') as f:
                f.write(providers_string)


def main():
    move_vpc_module_in_networking()
    region_in_main_region()
    print('asdsadsa')
main()

#
#variable "vpc_cidr" {
#  description = "Provide vpc cidr block"
#  type        = string
#}

#variable "vpc_az_count" {
#  description = "Provide number of az to use"
#  type        = number
#  default     = "2"
#}

#variable "vpc1_main_region" {
#  description = "Map for VPC configuration of the main region"
#  type = set(object({
#    cidrs    = list(string),
#    az_count = number
#  }))
#  default = null
#}

# from
#vpc_cidr       = var.vpc_cidr
#vpc_stage  = var.vpc_stage +
#vpc_az_count = var.vpc_az_count
# to
#vpc_cidr       = each.key
#vpc_stage  = var.vpc_stage +
#vpc_az_count = each.value.az_count

# network_provider_role_arn = "arn:aws:iam::877687016832:role/pg-role-gitlab-infra-automation"