
locals {
  vpc_main_region = flatten([
    for object_element in var.vpc_main_region : [
      for cidr in object_element.cidrs :
      {
        "az_count" = object_element.az_count
        "cidr"     = cidr
      }
    ]
  ])
}
module "vpc" {
  for_each = {
    for k in local.vpc_main_region : k.cidr => k
  }

  source = "git::https://code.n3yron.com/infrastructure/tf-modules/terraform-aws-module-vpc.git?ref=v3.0.0"

  vpc_cidr = each.key
  vpc_stage = var.vpc_stage
  vpc_az_count = each.value.az_count


  route53_rule_id      = var.route53
  route53_query_log_id = var.route53_query_og_id
  dns_query_53         = var.dns_query_53
  enable_route53_phz   = var.enable_route53_phz
  route53_phz_prd_id   = var.route53_phz_prd_id
  route53              = var.route53
  route531             = var.route53

  providers = {
    aws         = aws
    aws.network = aws.network
  }
}
