import os, re
with open('variables.tf', 'r') as f:
    text = f.read()
print(text)

match = re.findall('\n\n', text, re.DOTALL)
#if match:
#    vpc_stage = match.group()
print(match)
terraform_code = re.sub(r'\n{3,}', '\n\n', text)
print(terraform_code)

with open('networking.tf', 'r') as f:
    text = f.read()
print('#####')
terraform_code = re.sub(r'\nlocals {', 'locals {', text)
print(terraform_code)