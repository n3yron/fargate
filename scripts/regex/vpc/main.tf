terraform {
  backend "s3" {
    bucket         = "terraform-backend-lambda-n3yron"
    key            = "cloudwatch/terraform.tfstate"
    region         = "us-west-2"
    dynamodb_table = "backend-lambda-n3yron"
  }
}

terraform {
  backend "lolkek" {
    bucket         = "terraform-backend-lambda-n3yron"
    key            = "cloudwatch/terraform.tfstate"
    region         = "us-west-2"
    dynamodb_table = "backend-lambda-n3yron"
  }
}

module "backup" {
  source = "sadsadas"

  route53_rule_id      = var.route53
  route53_query_log_id = var.route53_query_og_id
  dns_query_53         = var.dns_query_53
  enable_route53_phz   = var.enable_route53_phz
  route53_phz_prd_id   = var.route53_phz_prd_id
  route53              = var.route53
  route531             = var.route53

  providers = {
    aws.network = aws.network
  }
}
