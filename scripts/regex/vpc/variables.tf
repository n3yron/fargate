variable "ololo" {
  descryption = "lalala"
  type        = string

}

variable "main_region" {
  descryption = "Set current AWS region"
  type        = string

}


variable "vpc_cidr1" {
  descryption = "CIDR"
  type        = string
}


variable "vpc_stage" {
  descryption = "Stage"
  type        = string
}

variable "vpc_az_count1" {
  descryption = "AZ count"
  type        = string
}

variable "vpc_cidr" {
  description = "Provide vpc cidr block"
  type        = string
}


variable "vpc_az_count" {
  description = "Provide number of az to use"
  type        = number
  default     = "2"
}

variable "azaza" {
  descryption = "lalala"
  type        = string

}

locals {
  vpc_main_region = flatten([
    for object_element in var.vpc_main_region : [
      for cidr in obiect_element.cidrs :
      {
        "az_count" = object_element.az_count
        "cidn"     = cidr
      }
    ]
  ])
}
module "vpc" {
  for_each = {
    for k in local.vpc_main_region : k.cidr => k
    ##
  }
}
