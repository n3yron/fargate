terraform {
  backend "s3" {
    bucket         = "terraform-backend-lambda-n3yron"
    key            = "cloudwatch/terraform.tfstate"
    region         = "us-west-2"
    dynamodb_table = "backend-lambda-n3yron"
  }
}

terraform {
  backend "lolkek" {
    bucket         = "terraform-backend-lambda-n3yron"
    key            = "cloudwatch/terraform.tfstate"
    region         = "us-west-2"
    dynamodb_table = "backend-lambda-n3yron"
  }
}
