import collections
import boto3
import json
import datetime
from datetime import timedelta
import sys
from botocore.client import Config ###


endtime = datetime.datetime.now()
interval = datetime.timedelta(days=3) #days=3
starttime = endtime - interval
config = Config(connect_timeout=1, retries={'max_attempts': 0})
client_lambda = boto3.client('lambda', config=config)

def lambda_handler():
    client = boto3.client('cloudtrail')
    paginator = client.get_paginator('lookup_events')
    page_iterator = paginator.paginate(LookupAttributes=[{'AttributeKey': 'ReadOnly', 'AttributeValue': 'false'}],StartTime=starttime, EndTime=endtime)
    for page in page_iterator:
        for event in page["Events"]:
            if 'Username' in event and 'n3yro' in event['Username']:
                print(event['Username'])
                print('Account in use.')
#                print(event)
                break
            elif event["EventName"] == "ConsoleLogin":
                print("2 - ololol")
                break
            elif event["EventName"] == "1CreateLogStream":
                print("3 - ololo")
                break
            else:
                response_1 = client_lambda.invoke(
                    FunctionName="lambda_2",
                    LogType='Tail',
                    InvocationType='Event'
                )
                #print(response_1)
                print(event)
                print('Account NOT in use. Nuked.')
                break
    
lambda_handler()