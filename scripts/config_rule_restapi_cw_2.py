import boto3

# Create a client object for API Gateway
client = boto3.client('apigateway')

# Step 1: List all APIs
apis = client.get_rest_apis()
api_ids = [api['id'] for api in apis['items']]

print(api_ids)
# Step 2: For each API, list all stages and check if "dataTraceEnabled": true
for api_id in api_ids:
    stages = client.get_stages(restApiId=api_id)
    for stage in stages['item']:
        #if "dataTraceEnabled" in stage:
        try:
            print(api_id)
            print(stage['methodSettings']['*/*']['dataTraceEnabled'])
        except KeyError:
            print("No dataTraceEnabled")
        #if 

        #if stage['methodSettings']['*/*']['loggingLevel']['dataTraceEnabled']:
        #    print('lol')
#        else:
#            print(f"API: {api_id}, Stage: {stage['stageName']}, dataTraceEnabled: False")

if no api gateway, then NOT_APPLICABLE
for api endoint in api endpoints
  if dataTraceEnabled true in stage:
    NON_COMPLIANT
  else
    COMPLIANT
###
def evaluate_compliance(event, configuration_item, valid_rule_parameters):
    evaluations = []
    apigateway_client = get_client('apigateway', event)
    # Step 1: List all APIs
    apis = apigateway_client.get_rest_apis()
    if not apis['items']:
        evaluations.append(build_evaluation(event['accountId'], 'NOT_APPLICABLE', event))

    for api in apis['items']:
        stages = client.get_stages(restApiId=api['id'])
        for stage in stages['item']:
            try:
                if stage['methodSettings']['*/*']['dataTraceEnabled'] == True:
                    #print(api['id']+'/stages/'+stage['stageName']+' : NON_COMPLIANT')
                    evaluations.append(build_evaluation(api['id'], 'NON_COMPLIANT', event, annotation='API GW have dataTraceEnabled=True'))
                elif stage['methodSettings']['*/*']['dataTraceEnabled'] == False:
                    #print(api['id']+'/stages/'+stage['stageName']+' : COMPLIANT')
                    evaluations.append(build_evaluation(api['id'], 'COMPLIANT', event, annotation='API GW have dataTraceEnabled=False'))
            except KeyError:
                print(api['id']+'/stages/'+stage['stageName']+": No methodSettings")

    return evaluations



    vpc_response = ec2_client.describe_vpcs() #if no VPC - NOT_APPLICABLE
    if not vpc_response['Vpcs']:
        evaluations.append(build_evaluation(event['accountId'], 'NOT_APPLICABLE', event))
        return evaluations
    for vpc in vpc_response['Vpcs']:
        evaluation_payload = get_vpcendpoints(vpc['VpcId'], event)
        evaluations.append(build_evaluation(vpc['VpcId'], evaluation_payload[vpc['VpcId']][1], event, annotation=evaluation_payload[vpc['VpcId']][0]))
    return evaluations
###
def evaluate_compliance(event, configuration_item, valid_rule_parameters):
    evaluations = []
    batch_client = get_client('batch', event)
    batch_response = batch_client.describe_compute_environments() # if no batch - NOT_APPLICABLE
    if not batch_response['computeEnvironments']:
        evaluations.append(build_evaluation(event['accountId'], 'NOT_APPLICABLE', event))
        return evaluations
    ec2_client = get_client('ec2', event)
    vpc_response = ec2_client.describe_vpcs() #if no VPC - NOT_APPLICABLE
    if not vpc_response['Vpcs']:
        evaluations.append(build_evaluation(event['accountId'], 'NOT_APPLICABLE', event))
        return evaluations
    for vpc in vpc_response['Vpcs']:
        evaluation_payload = get_vpcendpoints(vpc['VpcId'], event)
        evaluations.append(build_evaluation(vpc['VpcId'], evaluation_payload[vpc['VpcId']][1], event, annotation=evaluation_payload[vpc['VpcId']][0]))
    return evaluations

def evaluate_parameters(rule_parameters):
    valid_rule_parameters = rule_parameters
    return valid_rule_parameters

def get_vpcendpoints(vpc_id, event):
    compliance_results = {}
    ec2_client = get_client('ec2', event)
    compliance = 'NON_COMPLIANT'
    region = get_region_from_config_arn(event)
    annotate = 'Account uses batch, but there are no batch VPC endpoints present in '+ vpc_id+'.'
    response = ec2_client.describe_vpc_endpoints(Filters=[
                {
                    'Name':'vpc-id',
                    'Values':[vpc_id]
                },
                {
                    'Name':'service-name',
                    'Values': ['com.amazonaws.'+region+'.batch']
                }])
    if response['VpcEndpoints'] != []:
        for vpce in response['VpcEndpoints']:
            endpoint_state = vpce['State']
            annotate = 'Account uses batch, but batch VPC endpoint is not in Available state '+vpc_id+'.'
            if is_available(endpoint_state):
                annotate = None
                compliance = 'COMPLIANT'
    compliance_results['%s' %(vpc_id)] = [annotate, compliance]
    return compliance_results

def get_region_from_config_arn(event):
    return event['configRuleArn'].split(':')[3]

def is_available(endpointstate):
    return endpointstate == 'available'

def get_client(service, event):
    if not ASSUME_ROLE_MODE:
        return boto3.client(service)
    credentials = get_assume_role_credentials(event["executionRoleArn"])
    return boto3.client(service, aws_access_key_id=credentials['AccessKeyId'],
                        aws_secret_access_key=credentials['SecretAccessKey'],
                        aws_session_token=credentials['SessionToken']
                       )