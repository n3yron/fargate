import boto3
import json
from botocore.client import Config

DEFAULT_RESOURCE_TYPE = 'AWS::Batch::Cluster'
my_session = boto3.session.Session()
my_region = my_session.region_name
client = boto3.client('batch')
vpc_client = boto3.client('ec2')
# Get list of batches, if esists
def get_list_of_batches():
    batch_compenv_list = []
    #batch_compenv_subnets_list = []
    paginator = client.get_paginator('describe_compute_environments')
    response_iterator = paginator.paginate()
    for page in response_iterator:
        for compenv in page['computeEnvironments']:
#            print(compenv)
            #batch_compenv_list.append(compenv['computeEnvironmentArn'])
            batch_compenv_list.append(compenv)
    return batch_compenv_list
# MAIN CODE. Check if batches exists. If no, return NOT_APPLICABLE. Else, check if batch vpc endpoints exists for batch vpcs
def get_compliance_evaluation():
    event = 'kek'
    evaluations = []
    batch_list = get_list_of_batches()
    if not batch_list:
        print("NOT_APPLICABLE")
        #evaluations.append(build_evaluation('lol', 'NOT_APPLICABLE', event))
    else:
        #evaluate_compliance()
        #print('batches exists')
        batch_compenv_list = get_list_of_batches()
    #    print(batch_compenv_list)
        for batch in batch_compenv_list:
            print(batch['computeEnvironmentName'])
            #print(batch['computeEnvironmentName'])
            #get batch subnet_0
            #print(batch['computeResources']['subnets'][0])
            #get vpc for this subnet_0
            describe_subnet_0 = vpc_client.describe_subnets(
                SubnetIds=[
                    batch['computeResources']['subnets'][0],
                ]
            )
            # get batch subnet_0 vpcID
            vpc_id = describe_subnet_0['Subnets'][0]['VpcId']
            #vpc_id = 'vpc-17b169bad99f80be1'
            #print(vpc_id)
            # check if VPC endpoints associated with this VPC
            response = vpc_client.describe_vpc_endpoints(
                Filters=[
                    {
                        'Name': 'vpc-id',
                        'Values': [
                            vpc_id,
                        ]
                    },
                    {
                        'Name': 'service-name',
                        'Values': [
                            'com.amazonaws.'+my_region+'.batch',
                        ]
                    },
                ],
            )
            #print(response['VpcEndpoints'])
            if not response['VpcEndpoints']:
                print('NOT_COMLIENT')
            else:
                print('COMPLIENT')
                evaluations.append(build_evaluation(batch['computeEnvironmentName'], 'COMPLIANT', event))
    print('Result !!!!!!!!!!!!!!!!!!!!!')
    #print(evaluations)
    return evaluations

##################################
# This generate an evaluation for config
def build_evaluation(resource_id, compliance_type, event, resource_type=DEFAULT_RESOURCE_TYPE, annotation=None):
    """Form an evaluation as a dictionary. Usually suited to report on scheduled rules.
    Keyword arguments:
    resource_id -- the unique id of the resource to report
    compliance_type -- either COMPLIANT, NON_COMPLIANT or NOT_APPLICABLE
    event -- the event variable given in the lambda handler
    resource_type -- the CloudFormation resource type (or AWS::::Account) to report on the rule (default DEFAULT_RESOURCE_TYPE)
    annotation -- an annotation to be added to the evaluation (default None). It will be truncated to 255 if longer.
    """
    eval_cc = {}
    if annotation:
        eval_cc['Annotation'] = build_annotation(annotation)
    eval_cc['ComplianceResourceType'] = resource_type
    eval_cc['ComplianceResourceId'] = resource_id
    eval_cc['ComplianceType'] = compliance_type
    eval_cc['OrderingTimestamp'] = 'time'
#    eval_cc['OrderingTimestamp'] = str(json.loads(event['invokingEvent'])['notificationCreationTime'])
#    print(eval_cc)
    return eval_cc

def build_annotation(annotation_string):
    if len(annotation_string) > 256:
        return annotation_string[:244] + " [truncated]"
    return annotation_string
##################################

def lambda_handler(event, context):
    print(event)
    # decode the aws confing response
    invoking_event = json.loads(event['invokingEvent'])
    configuration_item = invoking_event["configurationItem"]
    # pass the configuration item to our method
    config = boto3.client('config')
    compliance_evaluation = get_compliance_evaluation()



    
    