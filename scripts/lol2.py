#import collections
import boto3
#import json
import datetime
#from datetime import timedelta
import sys
from botocore.client import Config


endtime = datetime.datetime.now()
interval = datetime.timedelta(days=6) #days=3
starttime = endtime - interval
config = Config(connect_timeout=1, retries={'max_attempts': 0})
client_lambda = boto3.client('lambda', config=config)

def lambda_handler():
    print(interval)
    client = boto3.client('cloudtrail')
    paginator = client.get_paginator('lookup_events')
    page_iterator = paginator.paginate(LookupAttributes=[{'AttributeKey': 'ReadOnly', 'AttributeValue': 'false'}],StartTime=starttime, EndTime=endtime)
    for page in page_iterator:
        for event in page["Events"]:
            print
            if 'Username' in event and '1lambda_1' in event['Username']:
                print(event['Username'])
                print(event["EventId"])
                return "lol"
            elif event["EventName"] == "CreateFunction20150331":
                print(event["EventName"])
                print(event["EventId"])
                return "kek"
    print('end')
    response_1 = client_lambda.invoke(
        FunctionName="lambda_2",
        LogType='Tail',
        InvocationType='Event'
    )
    print(response_1)
lambda_handler()