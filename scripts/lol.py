import collections
import boto3
import json
import datetime
from datetime import timedelta

cloudtrail = boto3.client('cloudtrail')
client = boto3.client('lambda')

def lambda_handler():
    response = client.invoke(
        FunctionName="lambda_2",
        LogType='Tail'
    )
    print(response)
lambda_handler()