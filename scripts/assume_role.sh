    output=$(aws sts assume-role --role-arn arn:aws:iam::299779690023:role/test_route53 --role-session-name test_route53 --output json)
    export AWS_ACCESS_KEY_ID=$(echo $output | jq -r '.Credentials.AccessKeyId')
    export AWS_SECRET_ACCESS_KEY=$(echo $output | jq -r '.Credentials.SecretAccessKey')
    export AWS_SESSION_TOKEN=$(echo $output | jq -r '.Credentials.SessionToken')
    aws sts get-caller-identity

###
    output=$(aws sts assume-role --role-arn arn:aws:iam::013773107376:role/OrganizationAccountAccessRole --role-session-name OrganizationAccountAccessRole --output json)
    export AWS_ACCESS_KEY_ID=$(echo $output | jq -r '.Credentials.AccessKeyId')
    export AWS_SECRET_ACCESS_KEY=$(echo $output | jq -r '.Credentials.SecretAccessKey')
    export AWS_SESSION_TOKEN=$(echo $output | jq -r '.Credentials.SessionToken')
    aws sts get-caller-identity
###
    output=$(aws sts assume-role --role-arn arn:aws:iam::013773107376:role/n3yron_readonly --role-session-name n3yron_readonly --output json)
    export AWS_ACCESS_KEY_ID=$(echo $output | jq -r '.Credentials.AccessKeyId')
    export AWS_SECRET_ACCESS_KEY=$(echo $output | jq -r '.Credentials.SecretAccessKey')
    export AWS_SESSION_TOKEN=$(echo $output | jq -r '.Credentials.SessionToken')
    aws sts get-caller-identity
