def get_vpcendpoints(vpc_id, event):
    compliance_results = {}
    ec2_client = get_client('ec2', event)
    compliance = 'NON_COMPLIANT'
    region = get_region_from_config_arn(event)
    annotate = 'There are no Amazon S3 VPC endpoints present in '+ vpc_id+'.'
    response = ec2_client.describe_vpc_endpoints(Filters=[
                {
                    'Name':'vpc-id',
                    'Values':[vpc_id]
                },
                {
                    'Name':'service-name',
                    'Values': ['com.amazonaws.'+region+'.batch']
                }])
    if response['VpcEndpoints'] != []:
        for vpce in response['VpcEndpoints']:
            endpoint_state = vpce['State']
            annotate = 'The Amazon S3 VPC endpoint is not in Available state '+vpc_id+'.'
            if is_available(endpoint_state):
                annotate = None
                compliance = 'COMPLIANT'
    compliance_results['%s' %(vpc_id)] = [annotate, compliance]
    return compliance_results

def get_region_from_config_arn(event):
    return event['configRuleArn'].split(':')[3]

def is_available(endpointstate):
    return endpointstate == 'available'

def evaluate_compliance(event, configuration_item, valid_rule_parameters):
    evaluations = []
    batch_client = get_client('batch', event)
    batch_response = batch_client.describe_compute_environments()
    if not batch_response['computeEnvironments']:
        evaluations.append(build_evaluation(event['accountId'], 'NOT_APPLICABLE', event))
        return evaluations
    ec2_client = get_client('ec2', event)
    vpc_response = ec2_client.describe_vpcs()
    if not vpc_response['Vpcs']:
        evaluations.append(build_evaluation(event['accountId'], 'NOT_APPLICABLE', event))
        return evaluations
    for vpc in vpc_response['Vpcs']:
        evaluation_payload = get_vpcendpoints(vpc['VpcId'], event)
        evaluations.append(build_evaluation(vpc['VpcId'], evaluation_payload[vpc['VpcId']][1], event, annotation=evaluation_payload[vpc['VpcId']][0]))
    return evaluations

def evaluate_parameters(rule_parameters):
    valid_rule_parameters = rule_parameters
    return valid_rule_parameters
