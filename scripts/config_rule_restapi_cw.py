import boto3

# Create a client object for API Gateway
client = boto3.client('apigateway')

# Step 1: List all APIs
apis = client.get_rest_apis()
api_ids = [api['id'] for api in apis['items']]


for api in apis['items']:
    stages = client.get_stages(restApiId=api['id'])
    for stage in stages['item']:
        try:
            #print(str(api['id'])+'\n###\n'+str(stage['stageName']+'\n###\n'+stage['dataTraceEnabled']))
            if stage['methodSettings']['*/*']['dataTraceEnabled'] == True:
                print(api['id']+'/stages/'+stage['stageName']+' : NON_COMPLIANT')
            elif stage['methodSettings']['*/*']['dataTraceEnabled'] == False:
                print(api['id']+'/stages/'+stage['stageName']+' : COMPLIANT')
            #print(stage['stageName'])
            #print(stage['methodSettings']['*/*']['dataTraceEnabled'])
        except KeyError:
            print(api['id']+'/stages/'+stage['stageName']+": No methodSettings")
        #try:
        #    #print(api_id)
        #    if stage['methodSettings']['*/*']['dataTraceEnabled'] == 'True':
        #        print(stage['methodSettings']['*/*']['dataTraceEnabled'])
        #except KeyError:
        #    print("No dataTraceEnabled")