import boto3
import json
from botocore.client import Config

def evaluate_compliance(configuration_item):
    return {
        "compliance_type": "NON_COMPLIANT",
        "annotation": "describe_security_groups failure on group Kukudu"
    }
        
def lambda_handler(event, context):
    print(event)
    # decode the aws confing response
    invoking_event = json.loads(event['invokingEvent'])
    configuration_item = invoking_event["configurationItem"]
    # pass the configuration item to our method
    evaluation = evaluate_compliance(configuration_item)
    config = boto3.client('config')
    response = config.put_evaluations(
        Evaluations=[
            {
                'ComplianceResourceType': invoking_event['configurationItem']['resourceType'],
                'ComplianceResourceId': invoking_event['configurationItem']['resourceId'],
                'ComplianceType': evaluation["compliance_type"],
                "Annotation": evaluation["annotation"],
                'OrderingTimestamp': invoking_event['configurationItem']['configurationItemCaptureTime']
            },
        ],
        ResultToken=event['resultToken'],
        TestMode=True)

###### Test event for Lambda
#{ 
#    "invokingEvent": "{\"configurationItem\":{\"configurationItemCaptureTime\":\"2016-02-17T01:36:34.043Z\",\"awsAccountId\":\"123456789012\",\"configurationItemStatus\":\"OK\",\"resourceId\":\"i-00000000\",\"ARN\":\"arn:aws:ec2:us-east-2:123456789012:instance/i-00000000\",\"awsRegion\":\"us-east-2\",\"availabilityZone\":\"us-east-2a\",\"resourceType\":\"AWS::EC2::Instance\",\"tags\":{\"Foo\":\"Bar\"},\"relationships\":[{\"resourceId\":\"eipalloc-00000000\",\"resourceType\":\"AWS::EC2::EIP\",\"name\":\"Is attached to ElasticIp\"}],\"configuration\":{\"foo\":\"bar\"}},\"messageType\":\"ConfigurationItemChangeNotification\"}",
#    "ruleParameters": "{\"myParameterKey\":\"myParameterValue\"}",
#    "resultToken": "myResultToken",
#    "eventLeftScope": false,
#    "executionRoleArn": "arn:aws:iam::123456789012:role/config-role",
#    "configRuleArn": "arn:aws:config:us-east-2:123456789012:config-rule/config-rule-0123456",
#    "configRuleName": "change-triggered-config-rule",
#    "configRuleId": "config-rule-0123456",
#    "accountId": "123456789012",
#    "version": "1.0"
#}