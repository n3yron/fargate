# Archive
data "archive_file" "lambda_get_secret_value" {
  type        = "zip"
  source_file = "${path.module}/lambda_get_secret/lambda_get_secret.py"
  output_path = "${path.module}/lambda_get_secret/lambda_get_secret.zip"
}

# Archive
data "archive_file" "lambda_get_secret_value" {
  type        = "zip"
  source_file = "${path.module}/lambda_get_secret/lambda_get_secret.py"
  output_path = "${path.module}/lambda_get_secret/lambda_get_secret.zip"
  "lol" = "kek"
}
}
  filename      = "${path.module}/lambda_get_secret/lambda_get_secret.zip"
  function_name = "lambda_get_secret"
  role          = aws_iam_role.role_lambda_get_secret_value.arn
  #  handler       = "lambda.lambda_handler"
  runtime          = "python3.9"
  handler          = "lambda_get_secret.get_secret"
  source_code_hash = filebase64sha256("${path.module}/lambda_get_secret/lambda_get_secret.zip")
  environment {
    variables = {
      BUCKET      = "lol"
      REGION_NAME = var.region
      SECRET_NAME = aws_secretsmanager_secret.secret.name
      #      SECRET_NAME = var.secret
      #      ECRET_NAME = aws_secretsmanager_secret.secret.
    }
  }
}
# IAM
resource "aws_iam_role" "role_lambda_get_secret_value" {
  name               = "lambda_get_secret_value_role"
  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy" "lambda_role_logs_policy_2" {
  name   = "LambdaRolePolicy_logs"
  role   = aws_iam_role.role_lambda_get_secret_value.id
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}
# Policy attachment
resource "aws_iam_role_policy_attachment" "lambda_policy_1_1" {
  role       = aws_iam_role.role_lambda_get_secret_value.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}
resource "aws_iam_role_policy_attachment" "lambda_policy_1_2" {
  role       = aws_iam_role.role_lambda_get_secret_value.name
  policy_arn = "arn:aws:iam::aws:policy/SecretsManagerReadWrite"
}
