import os
filename = "messages.txt"
entries = {}

with open(filename, 'r') as file:
    for line in file:
        entry = line.strip()
        if entry in entries:
            entries[entry] += 1
        else:
            entries[entry] = 1

for entry, count in entries.items():
    print(f"{entry}: {count}")