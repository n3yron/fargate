resource "aws_cloudwatch_log_metric_filter" "my_metric_filter" {
  count          = length(var.filter_var) > 0 ? length(var.filter_var) : 0
  name           = "${var.filter_var[count.index].name}_count"
  pattern        = var.filter_var[count.index].pattern
  log_group_name = var.filter_var[count.index].log_group_name

  metric_transformation {
    name      = var.filter_var[count.index].metric_transformation_name
    namespace = var.cw_namespace
    value     = "1"
  }
}
#  disabled_reason       = var.fsbp_var[count.index].reason
variable "filter_var" {
  type = list(object({
    name                       = string
    log_group_name             = string
    metric_transformation_name = string
    pattern                    = string
  }))
  default = [
    { name : "cis_1", log_group_name : "cis_1", metric_transformation_name : "EventCount1", pattern : "ERROR" },
    { name : "cis_2", log_group_name : "cis_2", metric_transformation_name : "EventCount2", pattern : "SUCCESS" },
    { name : "cis_3", log_group_name : "cis_3", metric_transformation_name : "EventCount3", pattern : "{($.errorCode=\"*UnauthorizedOperation\") || ($.errorCode=\"AccessDenied*\")}" }
  ]
}

resource "aws_cloudwatch_metric_alarm" "cw_metric_alarm" {
  count      = length(var.alarm_var) > 0 ? length(var.alarm_var) : 0
  alarm_name = var.alarm_var[count.index].alarm_name
  #  alarm_name          = element(var.xxx, count.index)
  #  alarm_name          = element(var.alarm_var.alarm_name, count.index)
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = var.alarm_var[count.index].evaluation_periods
  threshold           = var.alarm_var[count.index].threshold
  period              = var.alarm_var[count.index].period
  statistic           = "Sum"
  metric_name         = var.alarm_var[count.index].metric_name
  namespace           = var.cw_namespace
  treat_missing_data  = "notBreaching"
}

variable "alarm_var" {
  type = list(object({
    alarm_name         = string
    evaluation_periods = number
    threshold          = number
    period             = number
    metric_name        = string
  }))
  default = [
    { alarm_name : "UnauthorizedAPINotification", evaluation_periods : "1", threshold : "1", period : "60", metric_name : "EventCount1" },
    { alarm_name : "2UnauthorizedAPINotification", evaluation_periods : "1", threshold : "1", period : "300", metric_name : "EventCount2" },
  ]
}

variable "cw_namespace" {
  default = "YourNamespace"
}
variable "xxx" {
  default = ["YourNamespace", "asdsadsa", "asdwqdd23223"]
}
/*
resource "aws_cloudwatch_metric_alarm" "alarm_get_all_vpcs_n3yron" {
  alarm_name                = "alarm_get_all_vpcs_n3yron"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  metric_name               = "EventCount"
  namespace                 = "YourNamespace"
  period                    = "60"
  statistic                 = "Sum"
  threshold                 = "1"
  alarm_description         = "This metric monitors ERROR"
  insufficient_data_actions = []
}
*/
