# https://registry.terraform.io/providers/hashicorp/aws/2.52.0/docs/resources/cloudwatch_log_group
resource "aws_cloudwatch_log_group" "cis_1" {
  name              = "cis_1"
  retention_in_days = 3

  tags = {
    Environment = "production"
    Application = "serviceA"
  }
}
resource "aws_cloudwatch_log_group" "cis_2" {
  name              = "cis_2"
  retention_in_days = 3

  tags = {
    Environment = "production"
    Application = "serviceA"
  }
}
resource "aws_cloudwatch_log_group" "cis_3" {
  name              = "cis_3"
  retention_in_days = 3

  tags = {
    Environment = "production"
    Application = "serviceA"
  }
}
# https://registry.terraform.io/providers/hashicorp/aws/2.52.0/docs/resources/cloudwatch_log_stream
resource "aws_cloudwatch_log_stream" "cis_1" {
  name           = "SampleLogStream1_cis1"
  log_group_name = aws_cloudwatch_log_group.cis_1.name
}
resource "aws_cloudwatch_log_stream" "cis_2" {
  name           = "SampleLogStream_cis2"
  log_group_name = aws_cloudwatch_log_group.cis_2.name
}
resource "aws_cloudwatch_log_stream" "cis_3" {
  name           = "SampleLogStream_cis3"
  log_group_name = aws_cloudwatch_log_group.cis_3.name
}
# https://registry.terraform.io/providers/hashicorp/aws/2.52.0/docs/resources/cloudwatch_log_metric_filter
/*
resource "aws_cloudwatch_log_metric_filter" "cis_1" {
  name           = "cis_1_count"
  pattern        = ""
  log_group_name = aws_cloudwatch_log_group.cis_1.name

  metric_transformation {
    name      = "EventCount"
    namespace = "YourNamespace"
    value     = "1"
  }
}
resource "aws_cloudwatch_log_metric_filter" "get_all_vpcs" {
  name           = "get_all_vpcs"
  pattern        = "ERROR"
  log_group_name = "/aws/lambda/get_all_vpcs"

  metric_transformation {
    name          = "EventCount"
    namespace     = "YourNamespace"
    value         = "1"
    default_value = "0"
  }
}
*/
####

####
/*
resource "aws_cloudwatch_metric_alarm" "alarm_get_all_vpcs_n3yron" {
  alarm_name                = "alarm_get_all_vpcs_n3yron"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  metric_name               = "EventCount"
  namespace                 = "YourNamespace"
  period                    = "60"
  statistic                 = "Sum"
  threshold                 = "1"
  alarm_description         = "This metric monitors ERROR"
  insufficient_data_actions = []
}
*/
module "log_metric_filter" {
  source       = "./modules/cw_log_metric_filter"
  cw_namespace = "kukudu"
  #  xxx          = ["asdsdas", "1asdasds", "xzcxzcsacc"]
}
#  env    = "dev"
#  vpc_cidr             = "10.100.0.0/16" # Each module should have corresponding variable inside module folder, to pass vars from other modules or root module
#  public_subnet_cidrs  = ["10.100.1.0/24", "10.100.2.0/24"]
#  private_subnet_cidrs = ["10.100.3.0/24", "10.100.4.0/24"]
variable "alarm_var1" {
  type = list(object({
    alarm_name         = string
    evaluation_periods = number
    threshold          = number
    period             = number
    metric_name        = string
  }))
  default = [
    { alarm_name : "UnauthorizedAPINotification", evaluation_periods : "1", threshold : "1", period : "60", metric_name : "EventCount1" },
    { alarm_name : "2UnauthorizedAPINotification", evaluation_periods : "1", threshold : "1", period : "300", metric_name : "EventCount2" },
  ]
}

variable "xxx1" {
  default = ["YourNamespace", "asdsadsa", "asdwqdd23223"]
}
