provider "aws" {
  region = var.region
}

provider "aws" {
  alias  = "alternate_region"
  region = "us-east-1"
}


resource "aws_cloudwatch_log_metric_filter" "my_metric_filter_2" {
  #  count          = length(var.filter_var) > 0 ? length(var.filter_var) : 0
  count          = var.enable_this == true ? 1 : 0
  name           = "asd_count"
  pattern        = "ERROR"
  log_group_name = "cis_1"

  metric_transformation {
    name      = "azaza"
    namespace = "YourNamespace"
    value     = "1"
  }
}


resource "aws_cloudwatch_metric_alarm" "cw_metric_alarm_2" {
  count      = var.enable_this == true ? 1 : 0
  alarm_name = "asd_count_alarm"
  #  alarm_name          = element(var.xxx, count.index)
  #  alarm_name          = element(var.alarm_var.alarm_name, count.index)
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  threshold           = "1"
  period              = "60"
  statistic           = "Sum"
  metric_name         = aws_cloudwatch_log_metric_filter.my_metric_filter_2[count.index].metric_transformation[0].name
  namespace           = "YourNamespace"
  treat_missing_data  = "notBreaching"
}

variable "enable_this" {
  type    = bool
  default = true
}

variable "lol" {
  type    = string
  default = "azaza"
}

variable "kek" {
  type    = string
  default = var.lol
}

output "kkk" {
  value = var.kek
}
