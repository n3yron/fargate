data "aws_vpcs" "foo" {}

output "foo" {
  value = data.aws_vpcs.foo
}

output "bar" {
  value = {
    for id in data.aws_vpcs.foo.ids : id => id
    if id != "vpc-07b169bad99f80be1"
  }
}
#output "bd_name" {
#  value = {
#    for k, bd in mso_schema_template_bd.bd : k => bd.name
#  }
#}
/*resource "aws_iam_user_ssh_key" "this" {
  for_each = {
    for name, user in var.users : name => user
    if user.ssh_public_key != ""
  }

  username   = each.key
  encoding   = "SSH"
  public_key = each.value.ssh_public_key

  depends_on = [aws_iam_user.this]
}
*/
