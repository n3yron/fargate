resource "aws_s3_bucket" "example" {
  bucket = "my-bucket-to-test-access-pilicies-1234567"
  tags   = local.common_tags
  depends_on = [
    local.common_tags
  ]
}

resource "aws_s3_bucket_policy" "allow_access_from_another_account" {
  bucket = aws_s3_bucket.example.id
  policy = data.aws_iam_policy_document.allow_access_from_another_account.json
}
# https://docs.aws.amazon.com/AmazonS3/latest/userguide/example-bucket-policies.html
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_policy
data "aws_iam_policy_document" "allow_access_from_another_account" {
  /*  statement {
    effect = "Allow"
    principals {
      type        = "AWS"
      identifiers = ["299779690023"]
    }
    actions = [
      "s3:GetObject",
      "s3:ListBucket",
    ]
    resources = [
      aws_s3_bucket.example.arn,
      "${aws_s3_bucket.example.arn}/*",
    ]
  }*/
  statement {
    effect = "Deny"

    principals {
      type        = "*"
      identifiers = ["*"]
    }

    #actions = ["s3:*"]
    actions = ["s3:Delete*"]
    #    actions = [
    #      "s3:GetObject",
    #      "s3:ListBucket",
    #    ]

    resources = [
      aws_s3_bucket.example.arn,
      "${aws_s3_bucket.example.arn}/*",
    ]
    condition {
      test     = "ForAnyValue:ArnNotEquals"
      variable = "aws:PrincipalArn"
      values   = ["arn:aws:iam::${data.aws_caller_identity.current.account_id}:user/n3yron"]
    } #299779690023
    condition {
      test     = "ForAnyValue:StringNotLike"
      variable = "aws:username"
      values   = ["n3yron1"]
    }
  }
}


#       test     = "ForAnyValue:StringNotLike"
#      variable = "aws:username"
#      values   = ["arn:aws:iam::299779690023:user/n3yron"]

/*
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::299779690023:root"
            },
            "Action": [
                "s3:ListBucket",
                "s3:GetObject"
            ],
            "Resource": [
                "arn:aws:s3:::my-bucket-to-test-access-pilicies-1234567/*",
                "arn:aws:s3:::my-bucket-to-test-access-pilicies-1234567"
            ]
        },
        {
            "Sid": "",
            "Effect": "Deny",
            "Principal": "*",
            "Action": [
                "s3:ListBucket",
                "s3:GetObject"
            ],
            "Resource": [
                "arn:aws:s3:::my-bucket-to-test-access-pilicies-1234567/*",
                "arn:aws:s3:::my-bucket-to-test-access-pilicies-1234567"
            ],
            "Condition": {
                "ForAnyValue:StringNotLike": {
                    "aws:username": "n3yron"
                }
            }
        }
    ]
}
*/
locals {
  # Common tags to be assigned to all resources
  common_tags = {
    Service = "asd"
    Owner   = "asd"
    Asd     = "asd"
  }
}

data "aws_caller_identity" "current" {}

output "account_id" {
  value = data.aws_caller_identity.current.account_id
}
