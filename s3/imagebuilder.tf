resource "aws_imagebuilder_image_pipeline" "example" {
  image_recipe_arn                 = aws_imagebuilder_image_recipe.example.arn
  infrastructure_configuration_arn = aws_imagebuilder_infrastructure_configuration.example.arn
  name                             = "example-123"

  schedule {
    schedule_expression = "cron(0 0 * * ? *)"
  }
}

#resource "aws_iam_instance_profile" "example" {
#  name = "EC2InstanceProfileForImageBuilder"
#  role = "EC2InstanceProfileForImageBuilder"
#}

resource "aws_imagebuilder_infrastructure_configuration" "example" {
  instance_profile_name         = "EC2InstanceProfileForImageBuilder"
  name                          = "example"
  terminate_instance_on_failure = true
  tags = {
    foo = "bar"
  }
}

resource "aws_imagebuilder_image_recipe" "example" {
  block_device_mapping {
    device_name = "/dev/xvdb"

    ebs {
      delete_on_termination = true
      volume_size           = 100
      volume_type           = "gp2"
    }
  }

  component {
    component_arn = "arn:aws:imagebuilder:us-west-2:299779690023:component/nessus-agent/1.0.2/1"
  }

  name         = "example"
  parent_image = "arn:${data.aws_partition.current.partition}:imagebuilder:${data.aws_region.current.name}:aws:image/amazon-linux-2-kernel-5-x86/x.x.x"
  version      = "1.0.0"

  # This configuration combines some "default" tags with optionally provided additional tags
  tags = merge(
    var.additional_tags,
    var.some_more_tags,
    {
      "Product"     = "Name",
      "Application" = "App",
      "Owner"       = "Email"
    },
  )
}

data "aws_partition" "current" {}

data "aws_region" "current" {}

variable "additional_tags" {
  default = {
    "lol" = "kek"
  }
  description = "Additional resource tags"
  type        = map(string)
}

variable "some_more_tags" {
  default     = {}
  description = "Additional resource tags"
  type        = map(string)
}

#resource "aws_secretsmanager_secret" "example" {
#  name = "example"
#}
