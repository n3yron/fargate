# terraform plan -destroy -target=aws_route53_record.www

/*resource "aws_route53_record" "www" {
  #  zone_id = aws_route53_zone.primary.zone_id
  zone_id = "Z06598873TM9XI29UXX28"
  name    = "a1.n3yron.click"
  type    = "A"
  ttl     = 60
  records = ["192.168.1.2"]

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_route53_record" "www2" {
  #  zone_id = aws_route53_zone.primary.zone_id
  zone_id = "Z06598873TM9XI29UXX28"
  name    = "a2.n3yron.click"
  type    = "A"
  ttl     = 60
  records = ["192.168.2.5"]

  lifecycle {
    create_before_destroy = true
  }
}
*/

data "aws_iam_policy_document" "proxy_ca" {
  statement {
    sid    = "AllowVPCEAccessToProxyCA"
    effect = "Allow"
    principals {
      type        = "AWS"
      identifiers = ["*"]
    }
    actions   = ["s3:GetObject"]
    resources = ["arn:aws:s3:::your-bucket-name/proxy_ca/*"]
    condition {
      test     = "StringEquals"
      variable = "aws:sourceVpce"
      values   = ["vpce-1234567890"]
    }
    condition {
      test     = "StringEquals"
      variable = "kms:EncryptionContext:service"
      values   = ["pi"]
    }
  }
  statement {
    sid    = "AllowVPCEAccessToProxyCA1"
    effect = "Allow"
    principals {
      type        = "AWS"
      identifiers = ["*"]
    }
    actions   = ["s3:GetObject"]
    resources = ["arn:aws:s3:::your-bucket-name/proxy_ca/*"]
    condition {
      test     = "StringEquals"
      variable = "aws:sourceVpce"
      values   = ["vpce-1234567890"]
    }
  }
}

#resource "aws_iam_policy" "example" {
#  name   = "example_policy"
#  path   = "/"
#  policy = data.aws_iam_policy_document.proxy_ca.json
#}
