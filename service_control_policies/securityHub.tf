#resource "aws_securityhub_account" "example" {}

#resource "aws_securityhub_standards_subscription" "cis" {
#  depends_on    = [aws_securityhub_account.example]
#  standards_arn = "arn:aws:securityhub:::ruleset/cis-aws-foundations-benchmark/v/1.2.0"
#}

/*resource "aws_securityhub_standards_subscription" "fsbp" {
  #  depends_on    = [aws_securityhub_account.example]
  standards_arn = "arn:aws:securityhub:us-east-1::standards/aws-foundational-security-best-practices/v/1.0.0"
}

resource "aws_securityhub_standards_control" "disable_fsbp" {
  count                 = length(var.fsbp_var)
  standards_control_arn = "arn:aws:securityhub:us-east-1:299779690023:control/aws-foundational-security-best-practices/v/1.0.0/${var.fsbp_var[count.index].name}"
  control_status        = "DISABLED"
  disabled_reason       = var.fsbp_var[count.index].reason
  depends_on            = [aws_securityhub_standards_subscription.fsbp]
}
variable "fsbp_var" {
  type = list(object({
    name   = string
    reason = string
  }))
  default = [
    {
      name   = "WAF.1"
      reason = "I want to disable"
    },
    {
      name   = "WAF.2"
      reason = "I want to disable, 2"
    },
    {
      name   = "ECS.1"
      reason = "One more reason"
    },
    {
      name   = "ECS.2"
      reason = "Simply want"
    }
  ]
}
*/
