resource "aws_organizations_policy" "example" {
  name = "example"

  content = <<CONTENT
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "PreventIAMUserWithLoginprofile",
      "Effect": "Deny",
      "Action": [
        "iam:ChangePassword",
        "iam:CreateLoginProfile",
        "iam:UpdateLoginProfile",
        "iam:UpdateAccountPasswordPolicy"
      ],
      "Resource": [
        "*"
      ]
    }
  ]
}
CONTENT
}
#resource "aws_organizations_policy_attachment" "unit" {
#  policy_id = aws_organizations_policy.example.id
#  target_id = aws_organizations_organizational_unit.stage.id
#}
resource "aws_organizations_policy" "deny_delete_cloudtrail" {
  name = "deny_delete_cloudtrail"

  content = <<CONTENT
{
	"Version": "2012-10-17",
	"Statement": [
		{
			"Sid": "Statement1",
			"Effect": "Deny",
			"Action": [
				"cloudtrail:DeleteTrail",
				"cloudtrail:StopLogging",
				"cloudtrail:RemoveTags",
				"cloudtrail:DeleteEventDataStore",
				"cloudtrail:DeleteServiceLinkedChannel"
			],
			"Resource": [
				"*"
			],
			"Condition": {
				"StringNotLike": {
					"aws:PrincipalArn": [
						"arn:aws:iam::299779690023:user/n3yron",
						"arn:aws:iam::299779690023:user/admin"
					]
				}
			}
		}
	]
}
CONTENT
}
#resource "aws_organizations_policy_attachment" "stage_cloudtrail" {
#  policy_id = aws_organizations_policy.deny_delete_cloudtrail.id
#  target_id = aws_organizations_organizational_unit.stage.id
#}


##########
#resource "aws_organizations_policy_attachment" "deny_non_tagged_resources" {
#  policy_id = aws_organizations_policy.deny_non_tagged_resources.id
#  target_id = "r-q8i8"
#}

resource "aws_organizations_policy" "deny_non_tagged_resources" {
  name = "deny_non_tagged_resources"
  type = "SERVICE_CONTROL_POLICY"

  content = data.aws_iam_policy_document.deny_non_tagged_resources.json
}
data "aws_iam_policy_document" "deny_non_tagged_resources" {
  statement {
    effect = "Deny"
    actions = [
      "ec2:*",
      "s3:*"
    ]
    resources = ["*"]
    #    condition {
    #      test     = "StringEquals"
    #      variable = "aws:RequestTag/Terraform"
    #      values   = ["yes"]
    #    }
    #   condition {
    #     test     = "StringEquals"
    #     variable = "aws:RequestTag/Terraform": "yes"
    #     values   = ["yes"]
    #   }


    #    condition {
    #      test     = "Null"
    #      variable = "aws:RequestTag"
    #      values   = ["Terraform*"]
    #    }

    condition {
      test     = "ForAnyValue:StringEquals"
      variable = "aws:RequestTag/Terraform"
      values   = ["yes"]
    }
    condition {
      test     = "ForAnyValue:ArnNotEquals"
      variable = "aws:PrincipalARN"
      values   = ["arn:aws:iam::013773107376:role/OrganizationAccountAccessRole"] # only this guy will be able to do this
    }
  }
}
# OrganizationAccountAccessRole arn:aws:iam::013773107376:role/OrganizationAccountAccessRole

#resource "aws_iam_policy" "example_policy" {
#  name   = "example-policy"
#  policy = data.aws_iam_policy_document.deny_non_tagged_resources.json
#}


#{
#  "Version": "2012-10-17",
#  "Statement": [
#    {
#      "Sid": "PreventIAMUserWithLoginprofile",
#      "Effect": "Deny",
#      "Action": [
#        "*"
#      ],
#      "Resource": [
#        "*"
#      ]
#	  "Condition": {
#		"StringEquals": {"aws:RequestTag/Terraform": "yes"}
#	  }
#    }
#  ]
#}


#resource "aws_organizations_policy_attachment" "stage_tags" {
#  policy_id = aws_organizations_policy.tag_policy.id
#  target_id = "r-q8i8"
#  #  target_id = aws_organizations_organizational_unit.stage.id
#}

resource "aws_organizations_policy" "tag_policy" {
  name        = "tag-policy-n3yron"
  type        = "TAG_POLICY"
  description = "ololo"

  content = <<DOC
{
    "tags": {
        "costcenter": {
            "tag_key": {
                "@@assign": "CostCenter"
            },
            "tag_value": {
                "@@assign": [
                    "100",
                    "200"
                ]
            },
            "enforced_for": {
                "@@assign": [
                    "secretsmanager:*"
                ]
            }
        }
    }
}
DOC
}


#resource "aws_servicequotas_service_quota" "example" {
#  quota_code   = "L-F678F1CE"
#  service_code = "vpc"
#  value        = 75
#}
