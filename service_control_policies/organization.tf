# Get Organization ID
data "aws_organizations_organization" "example" {}

output "org_ololo" {
  value = data.aws_organizations_organization.example.id
}
data "aws_organizations_organizational_units" "ou" {
  parent_id = data.aws_organizations_organization.example.roots[0].id
}

output "au_account_ids" {
  value = data.aws_organizations_organization.example.accounts[*].id
}
# Create OU "Stage"
resource "aws_organizations_organizational_unit" "stage" {
  name      = "stage"
  parent_id = data.aws_organizations_organization.example.roots[0].id
}

locals {
  account_ids = {
    for id in data.aws_organizations_organization.example.accounts[*].id : id => id
    if id != var.management_account
  }
}

output "out_ids" {
  value = local.account_ids
}

#count = length(var.user_names)


variable "alternate_contacts" {
  description = "Alternate contacts"
  type        = list(string)
  default     = ["operations", "billing", "security"]
}

data "aws_caller_identity" "current" {}

/*variable "alternate_contact_types" {
  description = "Alternate contact types"
  type        = list(string)
  default     = ["OPERATIONS", "BILLING", "SECURITY"]
}
*/
#variable "alternate_contact_types" {
#  description = "Alternate contact types"
#  type        = list(string)
#  default     = ["OPERATIONS", "BILLING", "SECURITY"]
#}
/*resource "aws_account_alternate_contact" "operations_production" {

  for_each = var.alternate_contact_types

  alternate_contact_type = each.value["name"]
  name                   = "operations_production"
  title                  = "Kukudu"
  email_address          = "${lower(each.value["name"])}+${lower(each.value["name"])}@operations.com"
  phone_number           = each.value["phone_number"]
}
*/

variable "alternate_contact_types" {
  type = map(object({
    name         = string
    phone_number = string
  }))
  default = {
    "operations" = {
      name         = "OPERATIONS"
      phone_number = "+111111111111"
    }
    "billing" = {
      name         = "BILLING"
      phone_number = "+111111111112"
    }
    "security" = {
      name         = "SECURITY"
      phone_number = "+111111111113"
    }
  }
}
