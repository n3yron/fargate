# API Gateway
resource "aws_api_gateway_rest_api" "upload_to_s3_bucket" {
  name = "upload_to_s3_bucket"
  endpoint_configuration {
    types = ["REGIONAL"]
  }
} ###
# root / resource method
# Need to add normally functioned responce
resource "aws_api_gateway_method" "root_method_s3" {
  rest_api_id   = aws_api_gateway_rest_api.upload_to_s3_bucket.id
  resource_id   = aws_api_gateway_rest_api.upload_to_s3_bucket.root_resource_id
  http_method   = "GET"
  authorization = "NONE"
}
########## Integraton response root
resource "aws_api_gateway_integration" "integration_root_s3" {
  rest_api_id             = aws_api_gateway_rest_api.upload_to_s3_bucket.id
  resource_id             = aws_api_gateway_rest_api.upload_to_s3_bucket.root_resource_id
  http_method             = aws_api_gateway_method.root_method_s3.http_method
  integration_http_method = "POST" # https://stackoverflow.com/questions/41371970/accessdeniedexception-unable-to-determine-service-operation-name-to-be-authoriz
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.lambda_upload_to_s3.invoke_arn
}
#resource "aws_api_gateway_resource" "root" {
#  path_part   = "get"
#  parent_id   = aws_api_gateway_rest_api.upload_to_s3_bucket.root_resource_id
#  rest_api_id = aws_api_gateway_rest_api.upload_to_s3_bucket.id
#}
resource "aws_lambda_permission" "upload_to_s3_bucket" {
  #  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.lambda_upload_to_s3.function_name
  principal     = "apigateway.amazonaws.com"
  # More: http://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-control-access-using-iam-policies-to-invoke-api.html
  source_arn = "arn:aws:execute-api:${var.myregion}:${var.accountId}:${aws_api_gateway_rest_api.upload_to_s3_bucket.id}/*/*/"
  #  source_arn = "arn:aws:execute-api:${var.myregion}:${var.accountId}:${aws_api_gateway_rest_upload_to_s3_bucket.api.id}/*/${aws_api_gateway_method.method2.http_method}${aws_api_gateway_resource.resource2.path}"
  #  source_arn = "${aws_api_gateway_rest_upload_to_s3_bucket.api.execution_arn}/*/*/*"
}
#################
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_stage
resource "aws_api_gateway_stage" "upload_to_s3_bucket" {
  deployment_id = aws_api_gateway_deployment.upload_to_s3_bucket.id
  rest_api_id   = aws_api_gateway_rest_api.upload_to_s3_bucket.id
  stage_name    = "prod"
}
# API Deployment
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_deployment
resource "aws_api_gateway_deployment" "upload_to_s3_bucket" {
  rest_api_id = aws_api_gateway_rest_api.upload_to_s3_bucket.id

  triggers = {
    # NOTE: The configuration below will satisfy ordering considerations,
    #       but not pick up all future REST API changes. More advanced patterns
    #       are possible, such as using the filesha1() function against the
    #       Terraform configuration file(s) or removing the .id references to
    #       calculate a hash against whole resources. Be aware that using whole
    #       resources will show a difference after the initial implementation.
    #       It will stabilize to only change when resources change afterwards.
    redeployment = sha1(jsonencode([
      #     aws_api_gateway_resource.upload_to_s3.id, ###
      aws_api_gateway_method.root_method_s3.id,
      aws_api_gateway_integration.integration_root_s3.id,
    ]))
  }

  lifecycle {
    create_before_destroy = true
  }
}
