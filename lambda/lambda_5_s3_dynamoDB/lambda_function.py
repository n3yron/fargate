# when file is uploaded on S3, this creating event, which triggering Lambda. boto3 is parsing event and creating entry in DynamoDB with this file data
import json
import urllib.parse
import boto3
import os

def lambda_handler(event, context):
    s3 = boto3.client('s3')
    # Get the object from the event and show its content type
    bucket = event['Records'][0]['s3']['bucket']['name']
    key = urllib.parse.unquote_plus(event['Records'][0]['s3']['object']['key'], encoding='utf-8')
#    key1 = urllib.parse.unquote_plus(event['Records'][0]['s3']['object'], encoding='utf-8')
    try:
        print(os.environ['DYNAMODB'])
        print(event['Records'][0]['s3']['object'])
        print(event['Records'][0]['s3']['object']['key'])
        print(event['Records'][0]['s3']['bucket'])
        dynamodb = boto3.client('dynamodb')
        dynamodb.put_item(TableName=os.environ['DYNAMODB'], Item={'FileName':{'S':event['Records'][0]['s3']['object']['key']},'Size':{'S':str(event['Records'][0]['s3']['object']['size'])},'Bucket':{'S':str(event['Records'][0]['s3']['bucket']['name'])}})
#        return response['ContentType']
    except Exception as e:
        print(e)
        print('Error getting object {} from bucket {}. Make sure they exist and your bucket is in the same region as this function.'.format(key, bucket))
        raise e
