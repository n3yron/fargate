resource "aws_dynamodb_table" "basic-dynamodb-table" {
  name           = "restapi-table-n3yron"
  billing_mode   = "PROVISIONED"
  read_capacity  = 20
  write_capacity = 20
  hash_key       = "FileName"
  range_key      = "Size"

  attribute {
    name = "FileName"
    type = "S"
  }

  attribute {
    name = "Size"
    type = "S"
  }

  attribute {
    name = "TopScore"
    type = "N"
  }

  #ttl {
  #  attribute_name = "TimeToExist"
  #  enabled        = false
  #}

  global_secondary_index {
    name               = "SizeIndex"
    hash_key           = "Size"
    range_key          = "TopScore"
    write_capacity     = 10
    read_capacity      = 10
    projection_type    = "INCLUDE"
    non_key_attributes = ["FileName"]
  }

  tags = {
    Name        = "dynamodb-table-1"
    Environment = "dev"
  }
}
