# IAM
resource "aws_iam_role" "role_lambda_5_s3_dynamodb" {
  name               = "lambda_5_s3_dynamodb_role"
  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy" "lambda_role_logs_policy_5" {
  name   = "LambdaRolePolicy_lambda_5_s3_dynamodb"
  role   = aws_iam_role.role_lambda_5_s3_dynamodb.id
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}
# Policy attachment
resource "aws_iam_role_policy_attachment" "lambda_policy_basic_exec" {
  role       = aws_iam_role.role_lambda_5_s3_dynamodb.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}
resource "aws_iam_role_policy_attachment" "lambda_policy_dynamodb" {
  role       = aws_iam_role.role_lambda_5_s3_dynamodb.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess"
}
resource "aws_iam_role_policy_attachment" "lambda_policy_s3" {
  role       = aws_iam_role.role_lambda_5_s3_dynamodb.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}
resource "aws_iam_role_policy_attachment" "lambda_policy_ses" {
  role       = aws_iam_role.role_lambda_5_s3_dynamodb.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonSESFullAccess"
}
