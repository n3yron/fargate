terraform {
  backend "s3" {
    bucket         = "terraform-backend-lambda-n3yron"
    key            = "lambda/terraform.tfstate"
    region         = "us-west-2"
    dynamodb_table = "backend-lambda-n3yron"
  }
}
# harmonic rnd
#terraform {
#  backend "s3" {
#    bucket         = "terraform-remote-state-aws-n3yron"
#    key            = "lambda/terraform.tfstate"
#    region         = "us-east-1"
#    dynamodb_table = "terraform-remote-state-lambda-n3yron-table"
#  }
#}
