# IAM
resource "aws_iam_role" "role_lambda_upload_to_s3" {
  name               = "lambda_upload_to_s3_role"
  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy" "role_lambda_upload_to_s3" {
  name   = "LambdaRolePolicy_lambda_upload_to_s3"
  role   = aws_iam_role.role_lambda_upload_to_s3.id
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}
# Policy attachment
resource "aws_iam_role_policy_attachment" "lambda_policy_basic_exec_upload_to_s3" {
  role       = aws_iam_role.role_lambda_upload_to_s3.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

resource "aws_iam_role_policy_attachment" "lambda_policy_s3_upload_to_s3" {
  role       = aws_iam_role.role_lambda_upload_to_s3.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}
