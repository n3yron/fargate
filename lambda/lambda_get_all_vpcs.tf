# Archive
data "archive_file" "lambda_get_all_vpcs" {
  type        = "zip"
  source_file = "${path.module}/lambda2/lambda_function.py"
  output_path = "${path.module}/lambda2/lambda_function.zip"
}
# Lambda
#resource "aws_lambda_permission" "apigw_lambda_get_all_vpcs" {
#  #  statement_id  = "AllowExecutionFromAPIGateway"
#  action        = "lambda:InvokeFunction"
#  function_name = aws_lambda_function.lambda_get_all_vpcs.function_name
#  principal     = "apigateway.amazonaws.com"
# More: http://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-control-access-using-iam-policies-to-invoke-api.html
#  source_arn = "arn:aws:execute-api:${var.myregion}:${var.accountId}:${aws_api_gateway_rest_api.api_get_all_vpcs.id}/*/*${aws_api_gateway_resource.resource2.path}"
#}

resource "aws_lambda_function" "lambda_get_all_vpcs" {
  filename      = "${path.module}/lambda2/lambda_function.zip"
  function_name = "get_all_vpcs"
  role          = aws_iam_role.role_lambda_get_all_vpcs.arn
  #  handler       = "lambda.lambda_handler"
  runtime = "python3.8"
  handler = "lambda_function.lambda_handler"
  #  source_code_hash = filebase64sha256("lambda.zip")
}

# IAM
resource "aws_iam_role" "role_lambda_get_all_vpcs" {
  name               = "lambda_get_all_vpcs_role"
  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy" "lambda_role_logs_policy_2" {
  name   = "LambdaRolePolicy_get_all_vpcs"
  role   = aws_iam_role.role_lambda_get_all_vpcs.id
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}
# Policy attachment
resource "aws_iam_role_policy_attachment" "lambda_policy_1_2" {
  role       = aws_iam_role.role_lambda_get_all_vpcs.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}
resource "aws_iam_role_policy_attachment" "lambda_policy_2_2" {
  role       = aws_iam_role.role_lambda_get_all_vpcs.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2FullAccess"
}
resource "aws_iam_role_policy_attachment" "lambda_policy_3_2" {
  role       = aws_iam_role.role_lambda_get_all_vpcs.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonVPCFullAccess"
}
