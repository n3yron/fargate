resource "aws_s3_bucket" "bucket" {

  bucket        = "tf-test-bucket-n3yron"
  force_destroy = true # https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket#force_destroy
  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }
}
resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = aws_s3_bucket.bucket.id

  lambda_function {
    lambda_function_arn = aws_lambda_function.lambda_5_s3_dynamodb.arn
    events              = ["s3:ObjectCreated:*"]
    #    filter_prefix       = "AWSLogs/"
    #    filter_suffix       = ".log"
  }

  depends_on = [aws_lambda_permission.allow_bucket]
}
resource "aws_lambda_permission" "allow_bucket" {
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.lambda_5_s3_dynamodb.arn
  principal     = "s3.amazonaws.com"
  source_arn    = aws_s3_bucket.bucket.arn
}
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_notification
