# https://registry.terraform.io/providers/hashicorp/aws/2.33.0/docs/resources/acm_certificate
resource "aws_acm_certificate" "cert" {
  domain_name       = var.domainName
  validation_method = "DNS"

  lifecycle {
    create_before_destroy = true
  }
}

data "aws_route53_zone" "zone" {
  name         = var.domainNameZone
  private_zone = false
}

resource "aws_route53_record" "cert_validation" {
  name            = tolist(aws_acm_certificate.cert.domain_validation_options)[0].resource_record_name
  type            = tolist(aws_acm_certificate.cert.domain_validation_options)[0].resource_record_type
  zone_id         = data.aws_route53_zone.zone.id
  records         = [tolist(aws_acm_certificate.cert.domain_validation_options)[0].resource_record_value]
  ttl             = 60
  allow_overwrite = true
}

resource "aws_acm_certificate_validation" "cert" {
  certificate_arn         = aws_acm_certificate.cert.arn
  validation_record_fqdns = ["${aws_route53_record.cert_validation.fqdn}"]
}


# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_domain_name
resource "aws_api_gateway_domain_name" "example" {
  domain_name              = var.domainName
  regional_certificate_arn = aws_acm_certificate_validation.cert.certificate_arn

  endpoint_configuration {
    types = ["REGIONAL"]
  }
}
# Example DNS record using Route53.
# Route53 is not specifically required; any DNS host can be used.
resource "aws_route53_record" "example" {
  name    = aws_api_gateway_domain_name.example.domain_name
  type    = "A"
  zone_id = data.aws_route53_zone.zone.id

  alias {
    evaluate_target_health = true
    name                   = aws_api_gateway_domain_name.example.regional_domain_name
    zone_id                = aws_api_gateway_domain_name.example.regional_zone_id
  }
}
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_base_path_mapping

resource "aws_api_gateway_base_path_mapping" "api_get_all_vpcs" {
  api_id      = aws_api_gateway_rest_api.api_get_all_vpcs.id
  stage_name  = aws_api_gateway_stage.api_get_all_vpcs.stage_name
  domain_name = aws_api_gateway_domain_name.example.domain_name
  base_path   = "vpc"
}
resource "aws_api_gateway_base_path_mapping" "upload_to_s3_bucket" {
  api_id      = aws_api_gateway_rest_api.upload_to_s3_bucket.id
  stage_name  = aws_api_gateway_stage.upload_to_s3_bucket.stage_name
  domain_name = aws_api_gateway_domain_name.example.domain_name
  base_path   = "s3"
}


