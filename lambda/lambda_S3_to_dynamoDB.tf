# Archive
data "archive_file" "lambda_5_s3_dynamodb" {
  type        = "zip"
  source_file = "${path.module}/lambda_5_s3_dynamoDB/lambda_function.py"
  output_path = "${path.module}/lambda_5_s3_dynamoDB/lambda_function.zip"
}
# Lambda
resource "aws_lambda_function" "lambda_5_s3_dynamodb" {
  filename      = "${path.module}/lambda_5_s3_dynamoDB/lambda_function.zip"
  function_name = "lambda_5_s3_dynamodb"
  role          = aws_iam_role.role_lambda_5_s3_dynamodb.arn
  #  handler       = "lambda.lambda_handler"
  runtime          = "python3.8"
  handler          = "lambda_function.lambda_handler"
  source_code_hash = filebase64sha256("${path.module}/lambda_5_s3_dynamoDB/lambda_function.zip")
  environment {
    variables = {
      DYNAMODB = aws_dynamodb_table.basic-dynamodb-table.id
    }
  }
}
