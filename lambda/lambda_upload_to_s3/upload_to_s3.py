# upload file to S3
import json
import urllib.parse
import boto3
import os
import urllib.request

myurl = "https://mobimg.b-cdn.net/v3/fetch/62/624e27fde335d49e2dd3c6b75c6027a3.jpeg"
bucket='tf-test-bucket-n3yron'
def lambda_handler(event, context):
    splitted_url = myurl.split('/')
    os.chdir('/tmp')
    urllib.request.urlretrieve(myurl, "/tmp/"+splitted_url[-1])
    print(splitted_url[-1])
    file_name=splitted_url[-1]
    file="/tmp/"+splitted_url[-1]
    s3 = boto3.resource('s3')
    s3.meta.client.upload_file(file, bucket, file_name)
    print(os.system('ls -lah /tmp/'))
    print('#######')

    responseObject = {}
    responseObject['statusCode'] = 200
    responseObject['headers'] = {}
    responseObject['headers']['Content-Type'] = 'application/json'
    responseObject['body'] = json.dumps(file)
    return(responseObject)