# Archive
data "archive_file" "lambda_upload_to_s3" {
  type        = "zip"
  source_file = "${path.module}/lambda_upload_to_s3/upload_to_s3.py"
  output_path = "${path.module}/lambda_upload_to_s3/upload_to_s3.zip"
}
# Lambda
resource "aws_lambda_function" "lambda_upload_to_s3" {
  filename      = "${path.module}/lambda_upload_to_s3/upload_to_s3.zip"
  function_name = "lambda_upload_to_s3"
  role          = aws_iam_role.role_lambda_upload_to_s3.arn
  #  handler       = "lambda.lambda_handler"
  runtime          = "python3.8"
  handler          = "upload_to_s3.lambda_handler"
  source_code_hash = filebase64sha256("${path.module}/lambda_upload_to_s3/upload_to_s3.zip")
  environment {
    variables = {
      BUCKET = aws_dynamodb_table.basic-dynamodb-table.id
    }
  }
}
