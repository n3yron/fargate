import json
from datetime import datetime
import os
import boto3

#myVpc = 'vos-360-hrnqa-sba-aws-eks-2-3-67475286-8ba6-4da9-ad3d-2356e31c-VPC'
# vpcId=vos-360-hrnqa-sba-aws-eks-2-3-67475286-8ba6-4da9-ad3d-2356e31c-VPC
def lambda_handler(event, context):
    transactionId = event['queryStringParameters']['message']
    vpcId = event['queryStringParameters']['vpcId']
    transactionResponse = {}
    transactionResponse['id'] = transactionId
    transactionResponse['vpcId'] = vpcId
    transactionResponse['message'] = 'Hello n3yron'

    ec2 = boto3.client('ec2')
    getVpc = ec2.describe_vpcs(
        Filters=[
            {
                'Name': 'tag:Name',
                'Values': [
                    vpcId,
                ]
            }
        ]
    )
#    return(response["Vpcs"][0]["VpcId"])
    transactionResponse['vpc'] = getVpc
    
    responseObject = {}
    responseObject['statusCode'] = 200
    responseObject['headers'] = {}
    responseObject['headers']['Content-Type'] = 'application/json'
    responseObject['body'] = json.dumps(transactionResponse)
#    responseObject['body'] = json.dumps(event)
    print('queryStringParameters:', json.dumps(event['queryStringParameters']))
    
    return(responseObject)
# sadsadas

# curl 'https://5d65m5irj8.execute-api.us-east-2.amazonaws.com/example/get?message=kukudu1'
# sadsadas

#import json, os, boto3

#def lambda_handler(event, context):
#    ec2 = boto3.client('ec2')
#    response = ec2.describe_vpcs(
#        Filters=[
#            {
#                'Name': 'tag:Name',
#                'Values': [
#                    'vos-360-hrnqa-sba-aws-eks-2-3-67475286-8ba6-4da9-ad3d-2356e31c-VPC',
#                ]
#            }
#        ]
#    )
#    return(response["Vpcs"][0]["VpcId"])
# https://stackoverflow.com/questions/47329675/boto3-how-to-check-if-vpc-already-exists-before-creating-it
