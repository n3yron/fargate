# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/wafv2_rule_group
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/wafv2_web_acl
resource "aws_wafv2_web_acl" "example" {
  name  = "web-acl-association-example"
  scope = "REGIONAL"

  default_action {
    allow {}
  }
  rule {
    name     = "rule-1"
    priority = 1

    override_action {
      count {}
    }

    statement {
      managed_rule_group_statement {
        name        = "AWSManagedRulesCommonRuleSet"
        vendor_name = "AWS"

        excluded_rule {
          name = "SizeRestrictions_QUERYSTRING"
        }

        excluded_rule {
          name = "NoUserAgent_HEADER"
        }

        scope_down_statement {
          geo_match_statement {
            country_codes = ["US", "NL"]
          }
        }
      }
    }
    visibility_config {
      cloudwatch_metrics_enabled = false
      metric_name                = "friendly-rule-metric-name"
      sampled_requests_enabled   = false
    }
  }

  visibility_config {
    cloudwatch_metrics_enabled = false
    metric_name                = "friendly-metric-name"
    sampled_requests_enabled   = false
  }
  count = var.enable_waf ? 1 : 0
}
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/wafv2_web_acl_association
resource "aws_wafv2_web_acl_association" "example" {
  resource_arn = aws_api_gateway_stage.api_get_all_vpcs.arn
  web_acl_arn  = aws_wafv2_web_acl.example[count.index].arn
  count        = var.enable_waf ? 1 : 0
}
#######
#resource "aws_wafv2_rule_group" "example" {
#  name     = "example-rule"
#  scope    = "REGIONAL"
#  capacity = 2

#  rule {
#    name     = "rule-1"
#    priority = 1

#    action {
#      allow {}
#    }

#    statement {

#      geo_match_statement {
#        country_codes = ["US", "NL"]
#      }
#    }

#    visibility_config {
#      cloudwatch_metrics_enabled = false
#      metric_name                = "friendly-rule-metric-name"
#      sampled_requests_enabled   = false
#    }
#  }

#  visibility_config {
#    cloudwatch_metrics_enabled = false
#    metric_name                = "friendly-metric-name"
#    sampled_requests_enabled   = false
#  }
#  count = var.enable_waf ? 1 : 0
#}
