# Get VPC by ID
# https://wp2jicrdhb.execute-api.us-east-2.amazonaws.com/example/get?vpcId=vpc-049826d7b37cd6197
import json
import boto3

def lambda_handler(event, context):
    vpcId = event['queryStringParameters']['vpcId']
    response = {}
    response['vpcId'] = vpcId
    ec2 = boto3.client('ec2')
    getVpc = ec2.describe_vpcs(
        VpcIds=[
            vpcId,
            ],
        )
    response['vpc'] = getVpc
    
    responseObject = {}
    responseObject['statusCode'] = 200
    responseObject['headers'] = {}
    responseObject['headers']['Content-Type'] = 'application/json'
    responseObject['body'] = json.dumps(response)

    return(responseObject)