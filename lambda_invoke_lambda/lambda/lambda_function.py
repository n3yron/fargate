import collections
import boto3
import json
import datetime
from datetime import timedelta
import sys

cloudtrail = boto3.client('cloudtrail')
client = boto3.client('lambda')
endtime = datetime.datetime.now()
interval = datetime.timedelta(days=3) #days=3
starttime = endtime - interval

def lambda_handler(event, context):
    response = cloudtrail.lookup_events(
        LookupAttributes=[
            {
                'AttributeKey': 'ReadOnly',
                'AttributeValue': 'false'
            },
        ],
        StartTime=starttime,
        EndTime=endtime,
#        MaxResults=20
    )
    print(response)
    print(endtime)
    print(interval)
    print(starttime)
    if 'ConsoleLogin' in str(response) or 'aws-go-sdk-' in str(response) or 'UpdateStack' in str(response):
        print("Account in usage")
        response_1 = client.invoke(
            FunctionName="lambda_2",
            LogType='Tail'
        )
        print(response_1)
        return json.dumps(response, default=str)
    else:
        print("Account NOT in usage")
        return json.dumps(response, default=str)
        sys.exit(1)

#lambda_handler()