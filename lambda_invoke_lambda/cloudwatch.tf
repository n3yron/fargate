resource "aws_cloudwatch_event_rule" "console" {
  name                = "lambda_1_scheduler"
  description         = "Run Lambda_1 each n times in a day"
  schedule_expression = "rate(1 minute)" # https://docs.aws.amazon.com/AmazonCloudWatch/latest/events/ScheduledEvents.html
}

resource "aws_cloudwatch_event_target" "lambda_1" {
  rule      = aws_cloudwatch_event_rule.console.name
  target_id = "InvokeLambda_1"
  arn       = aws_lambda_function.lambda_1.arn
}

resource "aws_lambda_permission" "allow_cloudwatch_to_call_lambda" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.lambda_1.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.console.arn
}
