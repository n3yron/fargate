# Archive
data "archive_file" "lambda_hello_world" {
  type        = "zip"
  source_file = "${path.module}/lambda/lambda_function.py"
  output_path = "${path.module}/lambda/lambda_function.zip"
}
# Lambda

resource "aws_lambda_function" "lambda_1" {
  filename      = "${path.module}/lambda/lambda_function.zip"
  function_name = "lambda_1"
  role          = aws_iam_role.role.arn
  #  handler       = "lambda.lambda_handler"
  runtime          = "python3.8"
  handler          = "lambda_function.lambda_handler"
  source_code_hash = filebase64sha256("./lambda/lambda_function.zip")
}

# IAM
resource "aws_iam_role" "role" {
  name               = "myrole"
  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy" "lambda_role_logs_policy" {
  name   = "LambdaRolePolicy"
  role   = aws_iam_role.role.id
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}
# Policy attachment
resource "aws_iam_role_policy_attachment" "lambda_policy_1" {
  role       = aws_iam_role.role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}
#resource "aws_iam_role_policy_attachment" "lambda_policy_2" {
#  role       = aws_iam_role.role.name
#  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2FullAccess"
#}
#resource "aws_iam_role_policy_attachment" "lambda_policy_3" {
#  role       = aws_iam_role.role.name
#  policy_arn = "arn:aws:iam::aws:policy/AmazonVPCFullAccess"
#}

resource "aws_iam_role_policy_attachment" "lambda_policy_4" {
  role       = aws_iam_role.role.name
  policy_arn = "arn:aws:iam::aws:policy/AWSCloudTrail_ReadOnlyAccess"
}

resource "aws_iam_role_policy_attachment" "lambda_policy_5" {
  role       = aws_iam_role.role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaRole"
}

#resource "aws_lambda_function_event_invoke_config" "example" {
#  function_name = aws_lambda_function.lambda_1.arn

#  destination_config {
#    on_success {
#      destination = aws_lambda_function.lambda_2.arn
#    }
#  }
#}
#################################################################################
#
#
# Lambda_2
#################################################################################

# Archive
data "archive_file" "lambda_2" {
  type        = "zip"
  source_file = "${path.module}/lambda/lambda_function_2.py"
  output_path = "${path.module}/lambda/lambda_function_2.zip"
}
# Lambda
resource "aws_lambda_function" "lambda_2" {
  filename      = "${path.module}/lambda/lambda_function_2.zip"
  function_name = "lambda_2"
  role          = aws_iam_role.role.arn
  #  handler       = "lambda.lambda_handler"
  runtime = "python3.8"
  handler = "lambda_function_2.lambda_handler"
  #  source_code_hash = filebase64sha256("./lambda_2/lambda_function.zip")
}
