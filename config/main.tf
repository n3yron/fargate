/*resource "aws_config_configuration_recorder" "check_batch_endpoint" {
  name     = "check_batch_endpoint"
  role_arn = aws_iam_role.r[0].arn
}

# AWS Config role
resource "aws_iam_role" "r" {
  count = var.enable_batch_endpoint_check == true ? 1 : 0
  name  = "awsconfig-check-batch-endpoint"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "config.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
POLICY
}
*/

# Archive
data "archive_file" "check_batch_endpoint" {
  count = var.enable_batch_endpoint_check == true ? 1 : 0
  type  = "zip"
  #  source_file = "${path.module}/lambda/check_apigw_trace.py"
  #  output_path = "${path.module}/lambda/check_apigw_trace.zip"
  source_file = "${path.module}/lambda/check_batch_endpoint.py"
  output_path = "${path.module}/lambda/check_batch_endpoint.zip"
}
# Lambda
resource "aws_lambda_function" "check_batch_endpoint" {
  count    = var.enable_batch_endpoint_check == true ? 1 : 0
  filename = "${path.module}/lambda/check_batch_endpoint.zip"
  #  filename      = "${path.module}/lambda/check_apigw_trace.zip"
  function_name = "pg-lambda-check-batch-endpoint"
  role          = aws_iam_role.check_batch_endpoint[0].arn
  runtime       = "python3.9"
  handler       = "check_batch_endpoint.lambda_handler"
  #handler          = "check_apigw_trace.lambda_handler"
  source_code_hash = data.archive_file.check_batch_endpoint[0].output_base64sha256
}

resource "aws_config_config_rule" "check_batch_endpoint" {
  count = var.enable_batch_endpoint_check == true ? 1 : 0
  name  = "CheckBatchEndpoint"
  source {
    owner             = "CUSTOM_LAMBDA"
    source_identifier = aws_lambda_function.check_batch_endpoint[0].arn
    source_detail {
      event_source = "aws.config"
      message_type = "ConfigurationItemChangeNotification"
      #      message_type                = "ScheduledNotification"
      #      maximum_execution_frequency = "TwentyFour_Hours"
    }
  }

  depends_on = [
    #    aws_config_configuration_recorder.check_batch_endpoint,
    aws_lambda_permission.check_batch_endpoint,
  ]
}

# Grant AWS Config to access Lambda
resource "aws_lambda_permission" "check_batch_endpoint" {
  count         = var.enable_batch_endpoint_check == true ? 1 : 0
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.check_batch_endpoint[0].function_name
  principal     = "config.amazonaws.com"
  statement_id  = "AllowExecutionFromConfig"
}

# IAM role for Lambda
resource "aws_iam_role" "check_batch_endpoint" {
  count              = var.enable_batch_endpoint_check == true ? 1 : 0
  name               = "pg-role-lambda-check-batch-endpoint"
  assume_role_policy = data.aws_iam_policy_document.check_batch_endpoint[0].json
  #  assume_role_policy = <<POLICY
  #{
  #  "Version": "2012-10-17",
  #  "Statement": [
  #    {
  #      "Action": "sts:AssumeRole",
  #      "Principal": {
  #        "Service": "lambda.amazonaws.com"
  #      },
  #      "Effect": "Allow",
  #      "Sid": ""
  #    }
  #  ]
  #}
  #POLICY
}
# sts for Lambda
data "aws_iam_policy_document" "check_batch_endpoint" {
  count = var.enable_batch_endpoint_check == true ? 1 : 0
  statement {
    sid     = ""
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

# Policy attachment AWSLambdaBasicExecutionRole for Lambda role
resource "aws_iam_role_policy_attachment" "lambda_basic_execution_for_check_batch_endpoint" {
  count      = var.enable_batch_endpoint_check == true ? 1 : 0
  role       = aws_iam_role.check_batch_endpoint[0].name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}
# Policy attachment for vpc and batch checking for Lambda role
resource "aws_iam_role_policy_attachment" "check_batch_endpoint_policy" {
  role       = aws_iam_role.check_batch_endpoint[0].name
  policy_arn = aws_iam_policy.check_batch_endpoint_policy[0].arn
}

resource "aws_iam_policy" "check_batch_endpoint_policy" {
  count = var.enable_batch_endpoint_check == true ? 1 : 0
  name  = "check_batch_endpoint-policy"
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      #      {
      #        Effect   = "Allow"
      #        Action   = ["s3:GetObject"]
      #        Resource = ["arn:aws:s3:::check_batch_endpoint-bucket/*"]
      #      },
      {
        Effect = "Allow"
        Action = [
          #          "config:*",
          "config:Put*",
          "config:Get*",
          "config:List*",
          "config:Describe*",
          "config:BatchGet*",
          "config:Select*"
        ]
        Resource = ["*"]
      },
      #      {
      #        Effect = "Allow"
      #        Action = [
      #          "s3:GetObject"
      #        ]
      #        Resource = ["arn:aws:s3:::*/AWSLogs/*/Config/*"]
      #      },
      {
        Sid    = "VPCReadOnly"
        Effect = "Allow"
        Action = [
          "ec2:DescribeVpc*"
          #          "ec2:Get*"
        ]
        Resource = ["*"]
      },
      {
        Sid    = "BatchReadOnly"
        Effect = "Allow"
        Action = [
          "batch:Describe*",
          "batch:List*"
        ]
        Resource = ["*"]
      },
      {
        Sid    = "CreateLogs"
        Effect = "Allow"
        Action = [
          "logs:CreateLogGroup",
          "logs:CreateLogStream",
          "logs:PutLogEvents"
        ]
        Resource = ["*"]
      },
      {
        Sid    = "APIGWReadOnly"
        Effect = "Allow"
        Action = [
          #          "apigateway:Get*"
          "apigateway:GetClientCertificate",
          "apigateway:GetClientCertificates",
          "apigateway:GetAccount",
          "apigateway:GetApiKey",
          "apigateway:GetApiKeys",
          "apigateway:GetAuthorizer",
          "apigateway:GetAuthorizers",
          "apigateway:GetBasePathMapping",
          "apigateway:GetBasePathMappings",
          "apigateway:GetDeployment",
          "apigateway:GetDeployments",
          "apigateway:GetDocumentationPart",
          "apigateway:GetDocumentationParts",
          "apigateway:GetDocumentationVersion",
          "apigateway:GetDocumentationVersions",
          "apigateway:GetDomainName",
          "apigateway:GetDomainNames",
          "apigateway:GetExport",
          "apigateway:GetGatewayResponse",
          "apigateway:GetGatewayResponses",
          "apigateway:GetIntegration",
          "apigateway:GetIntegrationResponse",
          "apigateway:GetMethod",
          "apigateway:GetMethodResponse",
          "apigateway:GetModel",
          "apigateway:GetModels",
          "apigateway:GetModelTemplate",
          "apigateway:GetRequestValidator",
          "apigateway:GetRequestValidators",
          "apigateway:GetResource",
          "apigateway:GetResources",
          "apigateway:GetRestApi",
          "apigateway:GetRestApis",
          "apigateway:GetSdk",
          "apigateway:GetSdkType",
          "apigateway:GetSdkTypes",
          "apigateway:GetStage",
          "apigateway:GetStages",
          "apigateway:GetTags",
          "apigateway:GetUsage",
          "apigateway:GetUsagePlan",
          "apigateway:GetUsagePlanKey",
          "apigateway:GetUsagePlanKeys",
          "apigateway:GetUsagePlans",
          "apigateway:GetVpcLink",
          "apigateway:GetVpcLinks",
          "execute-api:*",
          "apigateway:Get*",
        ]
        Resource = ["*"]
      },
    ]
  })
}
#################
resource "aws_config_config_rule" "r" {
  name = "kinesis-check"

  source {
    owner             = "AWS"
    source_identifier = "KINESIS_STREAM_ENCRYPTED"
  }
  scope {
    compliance_resource_types = ["AWS::Kinesis::Stream"]
  }

}
