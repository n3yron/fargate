resource "null_resource" "deregister_ami" {
  for_each = var.ami_ids
  triggers = {
    ami_id = each.value
  }

  provisioner "local-exec" {
    #    command = "aws ec2 deregister-image --image-id ${each.value} --region us-east-1"
    command = <<EOT
    output=$(aws sts assume-role --role-arn arn:aws:iam::299779690023:role/kukudu-role --role-session-name kukudu --output json)
    export AWS_ACCESS_KEY_ID=$(echo $output | jq -r '.Credentials.AccessKeyId')
    export AWS_SECRET_ACCESS_KEY=$(echo $output | jq -r '.Credentials.SecretAccessKey')
    export AWS_SESSION_TOKEN=$(echo $output | jq -r '.Credentials.SessionToken')
    aws sts get-caller-identity
    aws ec2 deregister-image --image-id ${each.value} --region us-east-1
    unset AWS_ACCESS_KEY_ID && unset AWS_SECRET_ACCESS_KEY && unset AWS_SESSION_TOKEN
    EOT
  }
}

variable "ami_ids" {
  type = map(string)
  default = {
    "image-1" = "ami-0d3ba266610f1734c"
    "image-2" = "ami-021587cae5b0e2c52"
  }
}
## need to assume role


