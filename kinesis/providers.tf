provider "aws" {
  region = var.region
}

provider "aws" {
  alias  = "alternate_region"
  region = "us-east-1"
}
