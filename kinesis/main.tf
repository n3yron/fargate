resource "aws_cloudwatch_log_destination" "test_destination" {
  name       = "n3yron_destination"
  role_arn   = "arn:aws:iam::299779690023:role/cloudwatch-test-n3yron"
  target_arn = aws_kinesis_firehose_delivery_stream.kinesis_for_cloudwatch.arn
}

resource "aws_cloudwatch_log_group" "example_kinesis_test" {
  name = "/aws/lambda/example_kinesis_test"

  tags = {
    Environment = "production"
    Application = "serviceA"
  }
}

resource "aws_cloudwatch_log_stream" "example_kinesis_test" {
  name           = "SampleLogStream1234"
  log_group_name = aws_cloudwatch_log_group.example_kinesis_test.name
}

resource "aws_cloudwatch_log_subscription_filter" "test_lambdafunction_logfilter" {
  name            = "test_lambdafunction_logfilter"
  role_arn        = "arn:aws:iam::299779690023:role/cloudwatch-test-n3yron"
  log_group_name  = aws_cloudwatch_log_group.example_kinesis_test.name
  filter_pattern  = ""
  destination_arn = aws_kinesis_firehose_delivery_stream.kinesis_for_cloudwatch.arn
  distribution    = "Random"
}

resource "aws_kinesis_firehose_delivery_stream" "kinesis_for_cloudwatch" {
  name        = "kinesis-firehose-n3yron"
  destination = "extended_s3"
  #  server_side_encryption {
  #    enabled = true
  #  }

  extended_s3_configuration {
    role_arn   = aws_iam_role.firehose_role.arn
    bucket_arn = aws_s3_bucket.bucket.arn
  }
}

resource "aws_s3_bucket" "bucket" {
  bucket        = "kinesis-n3yron"
  force_destroy = true
}

resource "aws_iam_role" "firehose_role" {
  name = "firehose_test_role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "firehose.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}
