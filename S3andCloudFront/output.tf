output "DNS_name" {
  value = aws_route53_record.example.fqdn
}
output "CloudFront_record" {
  value = aws_cloudfront_distribution.s3_distribution.domain_name
}
