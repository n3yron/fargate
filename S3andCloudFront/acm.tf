# https://registry.terraform.io/providers/hashicorp/aws/2.33.0/docs/resources/acm_certificate
resource "aws_acm_certificate" "cert" {
  domain_name       = var.domainName
  validation_method = "DNS"
  provider          = aws.alternate_region # SSL certificates for CloudFront require us-east-1
  lifecycle {
    create_before_destroy = true
  }
}
resource "aws_route53_record" "cert_validation" {
  name            = tolist(aws_acm_certificate.cert.domain_validation_options)[0].resource_record_name
  type            = tolist(aws_acm_certificate.cert.domain_validation_options)[0].resource_record_type
  zone_id         = data.aws_route53_zone.zone.id
  records         = [tolist(aws_acm_certificate.cert.domain_validation_options)[0].resource_record_value]
  ttl             = 60
  allow_overwrite = true
  provider        = aws.alternate_region
}

resource "aws_acm_certificate_validation" "cert" {
  certificate_arn         = aws_acm_certificate.cert.arn
  validation_record_fqdns = ["${aws_route53_record.cert_validation.fqdn}"]
  provider                = aws.alternate_region # SSL certificates for CloudFront require us-east-1
}
