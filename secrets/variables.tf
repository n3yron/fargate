variable "accountId" {
  default     = "299779690023"
  description = "Account ID"
}
variable "region" {
  default     = "us-west-2"
  description = "AWS region"
}
variable "account_aws" {
  default = "299779690023"
}
variable "domainName" {
  default = "kukudu123.n3yron.click"
}
variable "domainNameZone" {
  default = "n3yron.click."
}
variable "S3bucketName" {
  default = "static-ws-n3yron"
}
variable "secret" {
  default = "my_secret_password"
}
variable "secret_name" {
  default = "my_secret"
}
