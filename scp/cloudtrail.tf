data "aws_iam_policy_document" "foo1" {
  statement {
    sid       = "AWSCloudTrailAclCheck201503199"
    effect    = "Allow"
    resources = ["arn:aws:s3:::aws-cloudtrail-logs-013773107376-ac1ccae3"]
    actions   = ["s3:GetBucketAcl"]

    condition {
      test     = "StringEquals"
      variable = "AWS:SourceArn"
      values   = ["arn:aws:cloudtrail:us-west-2:299779690023:trail/lol"]
    }

    principals {
      type        = "Service"
      identifiers = ["cloudtrail.amazonaws.com"]
    }
  }

  statement {
    sid       = "AWSCloudTrailWrite201503193"
    effect    = "Allow"
    resources = ["arn:aws:s3:::aws-cloudtrail-logs-013773107376-ac1ccae3/AWSLogs/299779690023/*"]
    actions   = ["s3:PutObject"]

    condition {
      test     = "StringEquals"
      variable = "s3:x-amz-acl"
      values   = ["bucket-owner-full-control"]
    }

    condition {
      test     = "StringEquals"
      variable = "AWS:SourceArn"
      values   = ["arn:aws:cloudtrail:us-west-2:299779690023:trail/lol"]
    }

    principals {
      type        = "Service"
      identifiers = ["cloudtrail.amazonaws.com"]
    }
  }

  statement {
    sid       = "AWSCloudTrailWrite201503194"
    effect    = "Allow"
    resources = ["arn:aws:s3:::aws-cloudtrail-logs-013773107376-ac1ccae3/AWSLogs/o-ej7lx78z5z/*"]
    actions   = ["s3:PutObject"]

    condition {
      test     = "StringEquals"
      variable = "s3:x-amz-acl"
      values   = ["bucket-owner-full-control"]
    }

    condition {
      test     = "StringEquals"
      variable = "AWS:SourceArn"
      values   = ["arn:aws:cloudtrail:us-west-2:299779690023:trail/lol"]
    }

    principals {
      type        = "Service"
      identifiers = ["cloudtrail.amazonaws.com"]
    }
  }

  statement {
    sid       = "AWSCloudTrailAclCheck201503198"
    effect    = "Allow"
    resources = ["arn:aws:s3:::aws-cloudtrail-logs-013773107376-ac1ccae3"]
    actions   = ["s3:GetBucketAcl"]

    condition {
      test     = "StringEquals"
      variable = "AWS:SourceArn"
      values   = ["arn:aws:cloudtrail:us-west-2:299779690023:trail/kukudu"]
    }

    principals {
      type        = "Service"
      identifiers = ["cloudtrail.amazonaws.com"]
    }
  }

  statement {
    sid       = "AWSCloudTrailWrite20150319"
    effect    = "Allow"
    resources = ["arn:aws:s3:::aws-cloudtrail-logs-013773107376-ac1ccae3/data_events/AWSLogs/299779690023/*"]
    actions   = ["s3:PutObject"]

    condition {
      test     = "StringEquals"
      variable = "s3:x-amz-acl"
      values   = ["bucket-owner-full-control"]
    }

    condition {
      test     = "StringEquals"
      variable = "AWS:SourceArn"
      values   = ["arn:aws:cloudtrail:us-west-2:299779690023:trail/kukudu"]
    }

    principals {
      type        = "Service"
      identifiers = ["cloudtrail.amazonaws.com"]
    }
  }

  statement {
    sid       = "AWSCloudTrailWrite201503192"
    effect    = "Allow"
    resources = ["arn:aws:s3:::aws-cloudtrail-logs-013773107376-ac1ccae3/data_events/AWSLogs/o-ej7lx78z5z/*"]
    actions   = ["s3:PutObject"]

    condition {
      test     = "StringEquals"
      variable = "s3:x-amz-acl"
      values   = ["bucket-owner-full-control"]
    }

    condition {
      test     = "StringEquals"
      variable = "AWS:SourceArn"
      values   = ["arn:aws:cloudtrail:us-west-2:299779690023:trail/kukudu"]
    }

    principals {
      type        = "Service"
      identifiers = ["cloudtrail.amazonaws.com"]
    }
  }
}

resource "aws_s3_bucket_policy" "foo1" {
  bucket = "aws-cloudtrail-logs-013773107376-ac1ccae3"
  policy = data.aws_iam_policy_document.foo1.json
}
