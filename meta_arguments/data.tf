data "aws_vpcs" "foo" {}

#output "foo" {
#  value = data.aws_vpcs.foo
#}

output "bar" {
  value = {
    for id in data.aws_vpcs.foo.ids : id => id
    if id == "vpc-0b35ce8eb4f70a541"
  }
}

data "aws_instances" "test" {
}

output "name" {
  value = data.aws_instances.test
}
output "bar2" {
  value = {
    for_each = {
      for id in data.aws_instances.test.ids : id => id
    }
  }
}
