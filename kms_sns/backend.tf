terraform {
  backend "s3" {
    bucket         = "terraform-backend-lambda-n3yron"
    key            = "kms_sns/terraform.tfstate"
    region         = "us-west-2"
    dynamodb_table = "backend-lambda-n3yron"
  }
}
