data "aws_kms_alias" "kms_sns" {
  name = "alias/n3yron/sns"
}

resource "aws_sns_topic" "user_updates" {
  name              = "sns-test"
  kms_master_key_id = data.aws_kms_alias.kms_sns.id
}

resource "aws_kms_key" "kms_sns" {
  description             = "KMS key 1"
  deletion_window_in_days = 7
  enable_key_rotation     = true
  policy                  = data.aws_iam_policy_document.kms_sns.json
}

resource "aws_kms_alias" "kms_sns" {
  name          = "alias/n3yron/sns"
  target_key_id = aws_kms_key.kms_sns.key_id
}

data "aws_iam_policy_document" "kms_sns" {

  statement {
    sid       = "Allow admin access to the key"
    effect    = "Allow"
    resources = ["*"]

    actions = ["*"]

    principals {
      type = "AWS"
      identifiers = [
        "arn:aws:iam::299779690023:root",
        "arn:aws:iam::299779690023:user/n3yron"
      ]
    }
  }

  statement {
    sid       = "Allow kms:ViaService"
    effect    = "Allow"
    resources = ["*"]

    actions = [
      "kms:Encrypt",
      "kms:Decrypt",
      "kms:ReEncrypt*",
      "kms:GenerateDataKey*",
      "kms:CreateGrant",
      "kms:ListGrants",
      "kms:DescribeKey",
      "kms:Describe*",
      "kms:Put*",
      "kms:Create*",
      "kms:Update*",
      "kms:Delete*",
      "kms:Get*",
      "kms:EnableKeyRotation",
      "kms:ListResourceTags"
    ]

    condition {
      test     = "StringEquals"
      variable = "kms:ViaService"
      values   = ["sns.us-west-2.amazonaws.com"]
    }

    condition {
      test     = "StringEquals"
      variable = "kms:CallerAccount"
      values   = ["299779690023"]
    }

    principals {
      type        = "AWS"
      identifiers = ["*"]
    }
  }
}
