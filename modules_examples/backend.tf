#terraform {
#  backend "s3" {
#    bucket         = "terraform-backend-lambda-n3yron"
#    key            = "modules/terraform.tfstate"
#    region         = "us-west-2"
#    dynamodb_table = "backend-lambda-n3yron"
#  }
#}
# harmonic rnd
terraform {
  backend "s3" {
    bucket         = "terraform-remote-state-aws-n3yron"
    key            = "fargate/terraform.tfstate"
    region         = "us-east-1"
    dynamodb_table = "terraform-remote-state-fargate-n3yron-table"
  }
}
/*
*/
