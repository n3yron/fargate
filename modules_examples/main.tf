provider "aws" {
  region = var.region
}
# Directory where module is located. 
# In this case all vars will be taken from module default vars
#module "vpc-default" {
#  source = "./modules/network"
#}

# Pass vars in module, to use instead of default
module "vpc-dev" {
  source               = "./modules/network"
  env                  = "dev"
  vpc_cidr             = "10.100.0.0/16" # Each module should have corresponding variable inside module folder, to pass vars from other modules or root module
  public_subnet_cidrs  = ["10.100.1.0/24", "10.100.2.0/24"]
  private_subnet_cidrs = ["10.100.3.0/24", "10.100.4.0/24"]
}
# Security group module
module "sg-dev" {
  source   = "./modules/security_group"
  env      = "dev"
  vpc_id   = module.vpc-dev.vpc_id # Each module should have corresponding variable inside module folder, to pass vars from other modules
  sg_rules = true
}
# EC2 module
/*
module "ec2_instance" {
  source = "github.com/terraform-aws-modules/terraform-aws-ec2-instance.git" # github as a source for module
  #  source  = "terraform-aws-modules/ec2-instance/aws"
  #  version = "~> 3.0"

  name = "single-instance"

  ami                    = data.aws_ami.my_image.id
  instance_type          = "t2.micro"
  key_name               = "user1"
  monitoring             = true
  vpc_security_group_ids = [module.vpc-dev.sg_id]
  subnet_id              = module.vpc-dev.private_subnet_ids[0]

  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
}
*/
# AMI
data "aws_ami" "my_image" {
  most_recent = true

  filter {
    name   = "name"
    values = ["*ubuntu-focal-20.04-amd64-server-20220914*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"]
}

# https://medium.com/@mitesh_shamra/module-in-terraform-920257136228

/*
# Create EC2
resource "aws_instance" "web" {
  ami                    = data.aws_ami.my_image.id
  instance_type          = "t2.medium"
  key_name               = data.aws_key_pair.key_pair.key_name
  vpc_security_group_ids = [aws_security_group.default.id] # Set of strings is required
  subnet_id              = aws_subnet.public_subnets["public_subnet_1"].id
  count                  = var.node_count

  tags = {
    Dept = var.dept_id
    Name = "ec2_${random_id.server.hex}"
  }
}
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/key_pair
data "aws_key_pair" "key_pair" {
  key_name = "DevOp-All"
}

resource "random_id" "server" {
  #  keepers = {
  # Generate a new id each time we switch to a new AMI id
  #    ami_id = var.ami_id
  #  }

  byte_length = 8
}
*/


output "dev_vpc_public_subnet_ids" {
  value = module.vpc-dev.public_subnet_ids
}
output "dev_vpc_private_subnet_ids" {
  value = module.vpc-dev.private_subnet_ids
}
output "dev_vpc_vpc_id" {
  value = module.vpc-dev.vpc_id
}
data "aws_vpc" "vpc" {
  id = module.vpc-dev.vpc_id
}

output "sg_id" {
  value = module.sg-dev.sg_id
}

#output "sg_id" {
#  value = try(aws_security_group.sg[0].id, "asd")
#}

#output "sg_id" {
#  value = length(aws_security_group.sg[0]) > 0 ? aws_security_group.sg[0].id : null
#}
