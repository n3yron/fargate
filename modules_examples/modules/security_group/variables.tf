# from port 80 to port 80. Ports number should be equal corresponding cidr number
variable "port" {
  default = [
    "80",
    "443",
    "22"
  ]
}

variable "cidr_blocks" {
  default = [
    ["0.0.0.0/0"],
    ["0.0.0.0/0"],
    ["10.0.0.0/16"]
  ]
}
variable "vpc_id" {
  default = "blablabla"
}
variable "env" {
  default = "dev"
}
variable "sg_rules" {
  description = "If set to true, it will create security group"
  type        = bool
  default     = false
}
