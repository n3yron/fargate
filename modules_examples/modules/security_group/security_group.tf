resource "aws_security_group" "sg" {
  count       = var.sg_rules ? 1 : 0
  name        = "main_sg"
  description = "My main SG"
  vpc_id      = var.vpc_id
  tags = {
    Name = "my_SG",
    LOL  = var.vpc_cidr
  }
}
resource "aws_security_group_rule" "sg_rules" {
  count             = var.sg_rules ? length(var.port) : 0
  type              = "ingress"
  from_port         = element(var.port, count.index)
  to_port           = element(var.port, count.index)
  protocol          = "tcp"
  cidr_blocks       = element(var.cidr_blocks, count.index)
  security_group_id = aws_security_group.sg[0].id
}

variable "vpc_cidr" {
  default = "asdsdasd"
}
