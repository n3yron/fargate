# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table_association
resource "aws_route_table_association" "public_routes" {
  count          = length(aws_subnet.private_subnets[*].id)
  route_table_id = aws_route_table.public_routes[count.index].id
  subnet_id      = element(aws_subnet.public_subnets[*].id, count.index)
}
resource "aws_route_table_association" "private_routes" {
  count          = length(aws_subnet.private_subnets[*].id)
  route_table_id = element(aws_route_table.private_routes[*].id, count.index)
  subnet_id      = element(aws_subnet.private_subnets[*].id, count.index)
}
