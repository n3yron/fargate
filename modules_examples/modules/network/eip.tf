resource "aws_eip" "nat" {
  count      = length(var.private_subnet_cidrs)
  vpc        = true
  depends_on = [aws_internet_gateway.gw]
}
