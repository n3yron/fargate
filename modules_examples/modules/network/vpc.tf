resource "aws_vpc" "main-vpc" {
  cidr_block           = var.vpc_cidr
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags = {
    Name  = "${var.cluster_name}-vpc"
    Owner = var.owner
    Dept  = var.dept_id
  }
}
