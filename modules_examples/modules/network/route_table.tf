# RT for public subnet
resource "aws_route_table" "public_routes" {
  count  = length(var.public_subnet_cidrs)
  vpc_id = aws_vpc.main-vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }
  tags = {
    Name  = var.vpc_name
    Owner = var.owner
  }
}
# RT for private subnet
resource "aws_route_table" "private_routes" {
  count  = length(var.private_subnet_cidrs)
  vpc_id = aws_vpc.main-vpc.id
  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat_gw1[count.index].id
  }
  tags = {
    Name  = var.vpc_name
    Owner = var.owner
  }
}
