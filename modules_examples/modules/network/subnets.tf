# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/availability_zones
# Declare the data source
data "aws_availability_zones" "available" {
  state = "available"
}
### 2 Public subnets ###
resource "aws_subnet" "public_subnets" {
  count                   = length(var.public_subnet_cidrs)
  vpc_id                  = aws_vpc.main-vpc.id
  cidr_block              = element(var.public_subnet_cidrs, count.index)
  availability_zone       = data.aws_availability_zones.available.names[count.index]
  map_public_ip_on_launch = true
  tags = {
    Name     = "${var.env}-public-${count.index + 1}"
    VPC_Name = var.vpc_name
    Owner    = var.owner
    Dept     = var.dept_id
    Type     = "public"
    # https://docs.aws.amazon.com/eks/latest/userguide/alb-ingress.html
    "kubernetes.io/cluster/${var.cluster_name}" = "shared"
    "kubernetes.io/role/elb"                    = 1
  }
}
### 2 Private subnets ###
resource "aws_subnet" "private_subnets" {
  count             = length(var.private_subnet_cidrs)
  vpc_id            = aws_vpc.main-vpc.id
  cidr_block        = element(var.private_subnet_cidrs, count.index)
  availability_zone = data.aws_availability_zones.available.names[count.index]
  tags = {
    Name     = "${var.env}-private-${count.index + 1}"
    VPC_Name = var.vpc_name
    Owner    = var.owner
    Dept     = var.dept_id
    Type     = "private"
    # https://docs.aws.amazon.com/eks/latest/userguide/alb-ingress.html
    "kubernetes.io/cluster/${var.cluster_name}" = "shared"
    "kubernetes.io/role/internal-elb"           = 1
  }
}
