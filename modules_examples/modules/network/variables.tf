variable "vpc_cidr" {
  default = "10.0.0.0/16"
}
variable "public_subnet_cidrs" {
  default = [
    "10.0.1.0/24",
    "10.0.2.0/24",
    "10.0.3.0/24"
  ]
}
variable "private_subnet_cidrs" {
  default = [
    "10.0.4.0/24",
    "10.0.5.0/24",
    "10.0.6.0/24"
  ]
}
variable "owner" {
  default = "devops"
}
variable "dept_id" {
  default = "4566"
}
variable "region" {
  default     = "us-west-2"
  description = "AWS region"
}
variable "cluster_name" {
  default = "n3yron-eks"
}
variable "account_aws" {
  default = "299779690023"
}
variable "vpc_name" {
  default = "n3yron-vpc"
}
variable "env" {
  default = "dev"
}
#locals {
#  region = "us-west-2"
#}
