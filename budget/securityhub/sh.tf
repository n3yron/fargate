data "aws_caller_identity" "current" {}

data "aws_region" "current" {}

resource "null_resource" "example1231" {
  count = var.enable_security_hub == true ? 1 : 0

  triggers = {
    #    cluster_instance_ids = "sad"
    auto_enable_controls = tostring(data.external.auto_enable_controls[count.index].result["AutoEnableControls"])
    #cluster_instance_ids = tostring(data.external.example.result["HubArn"])
  }
  provisioner "local-exec" {
    command = <<EOT
    output=$(aws sts assume-role --role-arn arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/kukudu-role --role-session-name kukudu --output json)
    export AWS_ACCESS_KEY_ID=$(echo $output | jq -r '.Credentials.AccessKeyId')
    export AWS_SECRET_ACCESS_KEY=$(echo $output | jq -r '.Credentials.SecretAccessKey')
    export AWS_SESSION_TOKEN=$(echo $output | jq -r '.Credentials.SessionToken')
    aws sts get-caller-identity && aws ec2 describe-vpcs --region ${data.aws_region.current.id}
    aws securityhub update-security-hub-configuration --no-auto-enable-controls --region ${data.aws_region.current.id}
    unset AWS_ACCESS_KEY_ID && unset AWS_SECRET_ACCESS_KEY && unset AWS_SESSION_TOKEN
    EOT
  }
}

data "external" "auto_enable_controls" {
  count = var.enable_security_hub == true ? 1 : 0
  #  program = ["sh", "-c", "controls_value=$(aws securityhub describe-hub  | jq -r .AutoEnableControls) && json='{\"AutoEnableControls\":\"'\"$controls_value\"'\"}' && echo $json | jq '.'"]
  program = [
    "sh",
    "-c",
    #    "date>/dev/null && cat ./securityhub/asd.sh",
    "output=$(aws sts assume-role --role-arn arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/kukudu-role --role-session-name kukudu --output json) && export AWS_ACCESS_KEY_ID=$(echo $output | jq -r '.Credentials.AccessKeyId') && export AWS_SECRET_ACCESS_KEY=$(echo $output | jq -r '.Credentials.SecretAccessKey') && export AWS_SESSION_TOKEN=$(echo $output | jq -r '.Credentials.SessionToken') && controls_value=$(aws securityhub describe-hub  --region ${data.aws_region.current.id} | jq -r .AutoEnableControls) && json='{\"AutoEnableControls\":\"'\"$controls_value\"'\"}' && echo $json | jq '.' && unset AWS_ACCESS_KEY_ID && unset AWS_SECRET_ACCESS_KEY && unset AWS_SESSION_TOKEN"
  ]
}


variable "enable_security_hub" {
  description = "enable_security_hub"
  type        = bool
  default     = true
}

terraform {
  required_version = "~> 1.2"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4"
      configuration_aliases = [
        aws.alternate_region
      ]
    }
  }
}

# aws sts assume-role --role-arn arn:aws:iam::299779690023:role/kukudu-role --role-session-name kukudu --output json
provider "aws" {
  alias  = "alternate_region"
  region = "us-east-1"
}

variable "sechub_region" {
  default = "us-east-1"
}

