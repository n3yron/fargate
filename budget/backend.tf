terraform {
  backend "s3" {
    bucket         = "terraform-backend-lambda-n3yron"
    key            = "secrets/terraform.tfstate"
    region         = "us-west-2"
    dynamodb_table = "backend-lambda-n3yron"
  }
}
