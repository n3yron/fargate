# SNS
resource "aws_sns_topic" "budget" {
  name = "budget-sns-topic"
}
resource "aws_sns_topic_policy" "default" {
  arn = aws_sns_topic.budget.arn

  policy = data.aws_iam_policy_document.sns_topic_policy.json
}

data "aws_iam_policy_document" "sns_topic_policy" {
  policy_id = "__default_policy_ID"

  statement {
    actions = [
      "SNS:Subscribe",
      "SNS:SetTopicAttributes",
      "SNS:RemovePermission",
      "SNS:Receive",
      "SNS:Publish",
      "SNS:ListSubscriptionsByTopic",
      "SNS:GetTopicAttributes",
      "SNS:DeleteTopic",
      "SNS:AddPermission",
    ]

    condition {
      test     = "StringEquals"
      variable = "AWS:SourceOwner"

      values = [
        var.accountId,
      ]
    }

    effect = "Allow"

    principals {
      type        = "AWS"
      identifiers = ["*"]
    }

    resources = [
      aws_sns_topic.budget.arn,
    ]

    sid = "__default_statement_ID"
  }
}

resource "aws_sns_topic_subscription" "email-target" {
  topic_arn = aws_sns_topic.budget.arn
  protocol  = "email"
  endpoint  = "n3yron@gmail.com"
}

data "aws_sns_topic" "example" {
  name       = "budget-sns-topic"
  depends_on = [aws_sns_topic.budget]
}
output "arn_sns" {
  value = data.aws_sns_topic.example.arn
}
#resource "aws_budgets_budget" "cost" {
#  name         = "budget-monthly"
#  budget_type  = "COST"
#  limit_amount = "4.35"
#  limit_unit   = "USD"
#  time_unit    = "MONTHLY"

#  notification {
#    comparison_operator = "GREATER_THAN"
#    threshold           = 4.35
#    threshold_type      = "ABSOLUTE_VALUE"
#    notification_type   = "ACTUAL"
#    subscriber_email_addresses = ["n3yron@gmail.com"]
#    subscriber_sns_topic_arns = ["${data.aws_sns_topic.example.arn}"]
#  }
#  depends_on = [data.aws_sns_topic.example]
#}

#resource "local_file" "foo" {
#  content  = "foo!"
#  filename = "${path.module}/foo.bar"
#}

#resource "null_resource" "example12" {
#  count = var.enable_security_hub == true ? 1 : 0
#  provisioner "local-exec" {
#    command = "output=$(aws sts assume-role --role-arn arn:aws:iam::299779690023:role/kukudu-role --role-session-name kukudu --output json) && export AWS_ACCESS_KEY_ID=$(echo $output | jq -r '.Credentials.AccessKeyId') && export AWS_SECRET_ACCESS_KEY=$(echo $output | jq -r '.Credentials.SecretAccessKey') && export AWS_SESSION_TOKEN=$(echo $output | jq -r '.Credentials.SessionToken') && aws sts get-caller-identity"
#    interpreter = ["perl", "-e"]
#  }
#}
data "aws_caller_identity" "current" {}
data "aws_region" "current" {}
#resource "null_resource" "example12345" {
#  count = var.enable_security_hub == true ? 1 : 0
#  provisioner "local-exec" {
#    command = "export AWS_ACCOUNT=${data.aws_caller_identity.current.account_id} && chmod +x ./files/test.sh && ./files/test.sh"
#    interpreter = ["perl", "-e"]
#  }
#}




/*variable "regions" {
  description = "List of regions"
  type        = list(string)
  default     = ["us-east-1", "us-east-2"]
}

locals {
  regions_enabled = var.enable_security_hub ? var.regions : []
}
resource "null_resource" "kukudu123" {

  for_each = toset(local.regions_enabled)

  provisioner "local-exec" {
    command = <<EOT
    output=$(aws sts assume-role --role-arn arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/kukudu-role --role-session-name kukudu --output json)
    export AWS_ACCESS_KEY_ID=$(echo $output | jq -r '.Credentials.AccessKeyId')
    export AWS_SECRET_ACCESS_KEY=$(echo $output | jq -r '.Credentials.SecretAccessKey')
    export AWS_SESSION_TOKEN=$(echo $output | jq -r '.Credentials.SessionToken')
    aws sts get-caller-identity && aws ec2 describe-vpcs --region ${each.value}
    unset AWS_ACCESS_KEY_ID && unset AWS_SECRET_ACCESS_KEY && unset AWS_SESSION_TOKEN
    EOT
  }
}
*/
module "lolkek" {
  source        = "./securityhub"
  sechub_region = var.sechub_region
  providers = {
    aws = aws.alternate_region
  }
}

module "lolkek_eu_central_1" {
  source = "./securityhub"
  #  sechub_region = "eu-central-1"
  #  providers = {
  #    aws = aws.alternate_region
  #  }
}
variable "sechub_region" {
  default = "us-east-1"
}

#provider = aws.alternate_region
#  vpc_id   = module.vpc-dev.vpc_id # Each module should have corresponding variable inside module folder, to pass vars from other modules


data "external" "auto_enable_controls" {
  #  program = ["cat", "./securityhub/asd.sh"]
  #  program = ["sh", "-c", "aws ec2 describe-vpcs | jq -r"]
  program = [
    "sh",
    "-c",
    #    "date>/dev/null && cat ./securityhub/asd.sh",
    "output=$(aws sts assume-role --role-arn arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/kukudu-role --role-session-name kukudu --output json) && export AWS_ACCESS_KEY_ID=$(echo $output | jq -r '.Credentials.AccessKeyId') && export AWS_SECRET_ACCESS_KEY=$(echo $output | jq -r '.Credentials.SecretAccessKey') && export AWS_SESSION_TOKEN=$(echo $output | jq -r '.Credentials.SessionToken') && controls_value=$(aws securityhub describe-hub  --region ${data.aws_region.current.id} | jq -r .AutoEnableControls) && json='{\"AutoEnableControls\":\"'\"$controls_value\"'\"}' && echo $json | jq '.'"
  ]
}

output "auto_enable_controls" {
  value = data.external.auto_enable_controls.result

}



