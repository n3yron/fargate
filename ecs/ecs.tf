locals {
  container_name = "nginx"
  container_port = "80"
}

# create backend with db and lb. With access via ALB
resource "aws_ecs_cluster" "this" {
  name = "n3yron-cluster"

  setting {
    name  = "containerInsights"
    value = "enabled"
  }
}

resource "aws_ecs_task_definition" "nginx" {
  family                   = "service"
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = 1024
  memory                   = 2048
  execution_role_arn       = "arn:aws:iam::299779690023:role/ecsTaskExecutionRole"
  container_definitions = jsonencode([
    {
      name  = "${local.container_name}"
      image = "docker.io/nginx:latest"
      #      cpu       = 1
      #      memory    = 1
      essential = true
      portMappings = [
        {
          containerPort = 80
          hostPort      = 80
        }
      ]
    }
  ])

  runtime_platform {
    operating_system_family = "LINUX"
    cpu_architecture        = "X86_64"
  }
  #  volume {
  #    name      = "service-storage"
  #    host_path = "/ecs/service-storage"
  #  }

  #  placement_constraints {
  #    type       = "memberOf"
  #    expression = "attribute:ecs.availability-zone in [us-west-2a, us-west-2b]"
  #  }
}

data "aws_ecs_task_definition" "nginx" {
  task_definition = aws_ecs_task_definition.nginx.family
}

resource "aws_ecs_service" "nginx" {
  name            = local.container_name
  cluster         = aws_ecs_cluster.this.id
  task_definition = aws_ecs_task_definition.nginx.arn
  launch_type     = "FARGATE"
  desired_count   = 1

  load_balancer {
    target_group_arn = aws_lb_target_group.this.arn
    container_name   = local.container_name
    container_port   = local.container_port
  }

  network_configuration {
    security_groups  = [aws_security_group.nginx.id, aws_security_group.this.id]
    subnets          = [for subnet in data.aws_subnet.example : subnet.id]
    assign_public_ip = true
  }

  #  depends_on = [
  #    aws_security_group.this
  #  ]
  #  placement_constraints {
  #    type       = "memberOf"
  #    expression = "attribute:ecs.availability-zone in [us-west-2a, us-west-2b]"
  #  }
}

