# VPC
resource "aws_vpc" "main" {
  cidr_block = "172.16.0.0/16"
}

/*variable "cidrs" {
  type    = list(string)
  default = ["172.16.0.0/24", "172.16.1.0/24"]
}
# Subnets
resource "aws_subnet" "main" {
  for_each   = toset(var.cidrs)
  vpc_id     = aws_vpc.main.id
  cidr_block = each.value

  tags = {
    Name = "Subnet"
  }
}
*/
# IGW
resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "main"
  }
}
# RT for public subnet
resource "aws_route_table" "gw" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  tags = {
    Name  = "lol"
    Owner = "kek"
  }
}

#resource "aws_route_table_association" "public_1" {
#  subnet_id      = aws_subnet.public_subnets["public_subnet_1"].id
#  route_table_id = aws_route_table.gw[0].id # as we have count
#}

/*resource "aws_route_table_association" "public" {
  count = length(var.cidrs)

  #  alternate_contact_type = each.value["name"]

  subnet_id      = element(aws_subnet.main.*.id, count.index)
  route_table_id = aws_route_table.gw.id
  depends_on = [
    aws_subnet.main,
    data.aws_subnet_ids.this
  ]
}
*/

variable "subnet_cidrs_public" {
  description = "Subnet CIDRs for public subnets (length must match configured availability_zones)"
  # this could be further simplified / computed using cidrsubnet() etc.
  # https://www.terraform.io/docs/configuration/interpolation.html#cidrsubnet-iprange-newbits-netnum-
  default = ["172.16.0.0/24", "172.16.1.0/24"]
  type    = list(string)
}

resource "aws_subnet" "public" {
  count = length(var.subnet_cidrs_public)

  vpc_id     = aws_vpc.main.id
  cidr_block = var.subnet_cidrs_public[count.index]
  #  availability_zone = var.availability_zones[count.index]
}

resource "aws_route_table_association" "public" {
  count = length(var.subnet_cidrs_public)

  subnet_id      = element(aws_subnet.public.*.id, count.index)
  route_table_id = aws_route_table.gw.id
}
